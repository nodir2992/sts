<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Settings;

class ContentController extends Controller
{

    /**
     * Displays FileManges page.
     * @return string
     */
    public function actionFiles()
    {
        return $this->render('files');
    }

    /**
     * Displays Site Setting page.
     * @return mixed
     */
    public function actionSetting()
    {
        if ($post = Yii::$app->request->post()) {
            foreach ($post as $key => $value) {
                if (($setting = Settings::findOne(['key' => $key])) !== null && !empty($value)) {
                    $setting->value = $value;
                    $setting->save();
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('views', 'Setting saved.'));
            return $this->refresh();
        }

        return $this->render('setting', [
            'model' => new Settings(),
            'models' => Settings::find()->all()
        ]);
    }

    /**
     * Action Create Site Setting.
     * @return mixed
     */
    public function actionSettingCreate()
    {
        $model = new Settings();
        ($model->load(Yii::$app->request->post()) && $model->save()) ?
            Yii::$app->session->setFlash('success', Yii::t('views', '{label} field added.', ['label' => $model->label])) :
            Yii::$app->session->setFlash('error', Yii::t('views', 'Field not added.'));

        return $this->redirect(['setting']);
    }

    /**
     * Deletes an existing Setting model.
     * @param integer $id
     * @return mixed
     */
    public function actionSettingDelete($id)
    {
        Settings::findOne($id)->delete();
        Yii::$app->session->setFlash('warning', Yii::t('views', 'Field deleted.'));

        return $this->redirect(['setting']);
    }
}
