<?php

namespace backend\controllers;

use Yii;
use yii\db\Query;
use yii\web\Controller;
use backend\models\order\Orders;

class ReportController extends Controller
{
    public function actionIndex()
    {
        $dataByStatus = (new Query())
            ->select('o.status, COUNT(DISTINCT o.id) as quantity, SUM(oi.total_price) as totalPrice')
            ->from('orders o')
            ->leftJoin('order_items oi', 'oi.order_id=o.id')
            ->where(['!=', 'o.status', Orders::STATUS_DELETED])
            ->groupBy('o.status')
            ->all();

        $dataByPayment = (new Query())
            ->select('o.payment, COUNT(DISTINCT o.id) as quantity, SUM(oi.total_price) as totalPrice')
            ->from('orders o')
            ->leftJoin('order_items oi', 'oi.order_id=o.id')
            ->where(['o.status' => Orders::STATUS_COMPLETED])
            ->groupBy('o.payment')
            ->all();

        $today = strtotime(date("Y-m-d"));
        $yesterday = $today - 86400;
        $week = $today - ((int)date("w") - 1) * 86400;
        $month = $today - ((int)date("d") - 1) * 86400;

        $dataDaily = [
            'today' => $this->getDailyReport($today),
            'yesterday' => $this->getDailyReport($yesterday, $today),
            'week' => $this->getDailyReport($week),
            'month' => $this->getDailyReport($month),
        ];

        $filterStartDate = $month;
        $filterEndDate = $today + 86399;
        $filterInputValueDefault = Yii::$app->formatter->asDate($filterStartDate, 'dd.MM.yyyy')
            . ' - ' . Yii::$app->formatter->asDate($filterEndDate, 'dd.MM.yyyy');
        $filterInputValue = $filterInputValueDefault;
        if ($get = Yii::$app->request->get()) {
            if ($filterInputValue = $get['filter_date']) {
                list($filterStartDate, $filterEndDate) = explode(' - ', $get['filter_date']);
                $filterStartDate = strtotime($filterStartDate);
                $filterEndDate = strtotime($filterEndDate) + 86400;
            }
        }

        $dataFilterStatus = (new Query())
            ->select('o.status, COUNT(DISTINCT o.id) as quantity, SUM(oi.total_price) as totalPrice')
            ->from('orders o')
            ->leftJoin('order_items oi', 'oi.order_id=o.id')
            ->where(['!=', 'o.status', Orders::STATUS_DELETED])
            ->andWhere(['between', 'o.created_at', $filterStartDate, $filterEndDate])
            ->groupBy('o.status')
            ->all();

        $dataFilterPayment = (new Query())
            ->select('o.payment, COUNT(DISTINCT o.id) as quantity, SUM(oi.total_price) as totalPrice')
            ->from('orders o')
            ->leftJoin('order_items oi', 'oi.order_id=o.id')
            ->where(['o.status' => Orders::STATUS_COMPLETED])
            ->andWhere(['between', 'o.created_at', $filterStartDate, $filterEndDate])
            ->groupBy('o.payment')
            ->all();

        $topClients = (new Query())
            ->select('u.id, u.email, COUNT(*) as qty, SUM(t.totalPrice) as total')
            ->from([
                't' => (new Query())
                    ->select('o.id, o.user_id, SUM(oi.total_price) as totalPrice')
                    ->from('orders o')
                    ->leftJoin('order_items oi', 'oi.order_id = o.id')
                    ->where(['o.status' => Orders::STATUS_COMPLETED])
                    ->andWhere(['!=', 'o.user_id', 0])
                    ->groupBy('oi.order_id')
            ])
            ->leftJoin('user u', 'u.id = t.user_id')
            ->groupBy('t.user_id')
            ->orderBy('total DESC')
            ->limit(20)
            ->all();

        $bestsellers = (new Query())
            ->select('p.id, p.name, SUM(oi.quantity) as qty')
            ->from('order_items oi')
            ->leftJoin('products p', 'p.id=oi.product_id')
            ->leftJoin('orders o', 'o.id=oi.order_id')
            ->where(['o.status' => Orders::STATUS_COMPLETED])
            ->groupBy('oi.product_id')
            ->orderBy('qty DESC')
            ->limit(20)
            ->all();

        return $this->render('index', [
            'dataByStatus' => $dataByStatus,
            'dataByPayment' => $dataByPayment,
            'dataDaily' => $dataDaily,
            'dataFilterStatus' => $dataFilterStatus,
            'dataFilterPayment' => $dataFilterPayment,
            'filterInputValue' => $filterInputValue,
            'filterInputValueDefault' => $filterInputValueDefault,
            'topClients' => $topClients,
            'bestsellers' => $bestsellers,
        ]);
    }

    /**
     * @param integer $start
     * @param integer|null $end
     * @return array
     */
    protected function getDailyReport($start, $end = null)
    {
        return (new Query())
            ->select('COUNT(DISTINCT o.id) as quantity, SUM(oi.total_price) as totalPrice')
            ->from('orders o')
            ->leftJoin('order_items oi', 'oi.order_id=o.id')
            ->where(['o.status' => Orders::STATUS_COMPLETED])
            ->andWhere($end ? ['between', 'o.created_at', $start, $end] : ['>', 'o.created_at', $start])
            ->one();
    }
}
