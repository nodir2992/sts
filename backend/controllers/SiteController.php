<?php
namespace backend\controllers;

use backend\models\menu\Menus;
use backend\models\order\Orders;
use backend\models\page\Pages;
use backend\models\parts\HtmlParts;
use backend\models\product\Brands;
use backend\models\product\Categories;
use backend\models\product\Products;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'newOrders' => count(Orders::findAll(['status' => Orders::STATUS_NEW])),
            'completedOrders' => count(Orders::findAll(['status' => Orders::STATUS_COMPLETED])),
            'allOrders' => count(Orders::find()->where(['!=', 'status', Orders::STATUS_DELETED])->all()),
            'brands' => count(Brands::find()->where(['!=', 'status', Brands::STATUS_DELETED])->all()),
            'categories' => count(Categories::find()->all()),
            'products' => count(Products::find()->where(['!=', 'status', Products::STATUS_DELETED])->all()),
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->can('accessDashboard')) {
                return $this->goBack();
            } else {
                Yii::$app->user->logout();
                $model->addError('password', 'Incorrect username or password.');
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Ajax Set Session SidebarCollapse.
     */
    public function actionSessionSidebar()
    {
        $session = Yii::$app->session;
        $session['sidebar'] = $session['sidebar'] ? false : true;
    }
}
