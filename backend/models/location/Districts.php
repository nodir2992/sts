<?php

namespace backend\models\location;

/**
 * This is extends model Locations.
 */
class Districts extends Locations
{

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->andWhere(['!=', 'status', self::STATUS_DELETED]);
    }
}
