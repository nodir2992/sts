<?php

namespace backend\models\product;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use backend\behaviors\SlugBehavior;
use backend\behaviors\MetaTitleBehavior;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $brand_id
 * @property int $group_id
 * @property int $discount_id
 * @property int $gift_id
 * @property string $barcode
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property string $information
 * @property string $detail
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $price
 * @property string $old_price
 * @property int $quantity
 * @property int $timer_start
 * @property int $timer_end
 * @property int $views
 * @property int $votes
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ProductImages[] $productImages
 * @property ProductToCategory[] $productToCategories
 * @property Categories[] $categories
 * @property ProductToFilter[] $productToFilters
 * @property FilterItems[] $filterItems
 * @property ProductToRibbon[] $productToRibbons
 * @property Ribbons[] $ribbons
 * @property Brands $brand
 * @property Discounts $discount
 * @property Products $gift
 * @property Groups $group
 */
class Products extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            MetaTitleBehavior::className(),
            SlugBehavior::className(),
            'sitemap' => [
                'class' => SitemapBehavior::class,
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'status', 'updated_at']);
                    $model->andWhere(['status' => [self::STATUS_AVAILABLE, self::STATUS_NOT_AVAILABLE]]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::to(['product/view', 'slug' => $model->slug], true),
                        'lastmod' => $model->updated_at,
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    public $filter_ids;
    public $ribbon_ids;
    public $ribbon_id;
    public $category_ids;
    public $category_id;
    public $uploaded_images;
    public $deleted_images;
    public $sorted_images;
    public $dataFilters;
    public $str_timer_start;
    public $str_timer_end;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'group_id', 'barcode', 'name', 'price'], 'required'],
            [['brand_id', 'group_id', 'discount_id', 'gift_id', 'quantity', 'timer_start', 'timer_end', 'views', 'votes', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description', 'information', 'detail', 'meta_keywords', 'meta_description'], 'string'],
            [['price', 'old_price'], 'number'],
            [['barcode'], 'string', 'max' => 64],
            [['slug', 'name', 'meta_title'], 'string', 'max' => 255],
            [['barcode'], 'unique'],
            [['slug'], 'unique'],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['category_ids'], 'required'],
            [['filter_ids', 'ribbon_ids', 'category_ids'], 'each', 'rule' => ['integer']],
            [['uploaded_images', 'deleted_images', 'sorted_images', 'str_timer_start', 'str_timer_end'], 'string'],
            [['ribbon_id', 'category_id'], 'integer'],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discounts::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['gift_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['gift_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'brand_id' => Yii::t('model', 'Brand'),
            'group_id' => Yii::t('model', 'Group'),
            'discount_id' => Yii::t('model', 'Discount'),
            'gift_id' => Yii::t('model', 'Gift'),
            'barcode' => Yii::t('model', 'Barcode'),
            'slug' => Yii::t('model', 'Slug'),
            'name' => Yii::t('model', 'Name'),
            'description' => Yii::t('model', 'Description'),
            'information' => Yii::t('model', 'Information'),
            'detail' => Yii::t('model', 'Detail'),
            'meta_title' => Yii::t('model', 'Meta Title'),
            'meta_keywords' => Yii::t('model', 'Meta Keywords'),
            'meta_description' => Yii::t('model', 'Meta Description'),
            'price' => Yii::t('model', 'Price'),
            'old_price' => Yii::t('model', 'Old Price'),
            'quantity' => Yii::t('model', 'Quantity'),
            'timer_start' => Yii::t('model', 'Timer Start'),
            'timer_end' => Yii::t('model', 'Timer End'),
            'views' => Yii::t('model', 'Views'),
            'votes' => Yii::t('model', 'Votes'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'filter_ids' => Yii::t('model', 'Filters'),
            'ribbon_ids' => Yii::t('model', 'Ribbons'),
            'ribbon_id' => Yii::t('model', 'Ribbon'),
            'category_ids' => Yii::t('model', 'Categories'),
            'category_id' => Yii::t('model', 'Category'),
            'uploaded_images' => Yii::t('model', 'Images'),
            'str_timer_start' => Yii::t('model', 'Timer Start'),
            'str_timer_end' => Yii::t('model', 'Timer End'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToCategories()
    {
        return $this->hasMany(ProductToCategory::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])->viaTable('product_to_category', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToFilters()
    {
        return $this->hasMany(ProductToFilter::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterItems()
    {
        return $this->hasMany(FilterItems::className(), ['id' => 'filter_item_id'])->viaTable('product_to_filter', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToRibbons()
    {
        return $this->hasMany(ProductToRibbon::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRibbons()
    {
        return $this->hasMany(Ribbons::className(), ['id' => 'ribbon_id'])->viaTable('product_to_ribbon', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discounts::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGift()
    {
        return $this->hasOne(Products::className(), ['id' => 'gift_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * Set Status Deleted
     * @return bool
     */
    public function setStatusDeleted()
    {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    /**
     * Status
     */
    const STATUS_AVAILABLE = 2;
    const STATUS_NOT_AVAILABLE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = -1;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_AVAILABLE => Yii::t('model', 'Available'),
            self::STATUS_NOT_AVAILABLE => Yii::t('model', 'Not Available'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_AVAILABLE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_AVAILABLE) . '</span>',
            self::STATUS_NOT_AVAILABLE => '<span class="text-bold text-warning">' . self::getStatusArray(self::STATUS_NOT_AVAILABLE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Is Available
     * @return bool
     */
    public function isAvailable()
    {
        return ($this->status === self::STATUS_AVAILABLE) ? true : false;
    }

    /**
     * Is Available
     * @return string
     */
    public function addToCartBtn()
    {
        return $this->isAvailable() ?
            Html::button(Yii::t('frontend', 'Заказать'), ['class' => 'btn add-to-cart', 'data-product' => $this->id]) :
            Html::button(Yii::t('frontend', 'Нет в наличии'), ['class' => 'btn product-not-available']);
    }

    /**
     * Dollar Rate
     * @return string
     */
    protected function getDollarRate()
    {
        /** @var $dollar DollarRate */
        return ($dollar = DollarRate::find()->orderBy(['id' => SORT_DESC])->one()) ? $dollar->value : 1;
    }

    /**
     * Price
     * @return string
     */
    public function getPriceBySum()
    {
        return $this->price * $this->getDollarRate();
    }

    /**
     * Price As Currency
     * @return string
     */
    public function getPriceAsCurrency()
    {
        return Yii::$app->formatter->asDecimal($this->getPriceBySum(), 0)
            . Yii::t('model', '&nbsp;<i>сум</i>');
    }

    /**
     * Old Price As Currency
     * @return string
     */
    public function getOldPriceAsCurrency()
    {
        return $this->old_price ? Yii::$app->formatter->asDecimal(($this->old_price * $this->getDollarRate()), 0)
            . Yii::t('model', '&nbsp;<i>сум</i>') : $this->old_price;
    }

    /**
     * Price As Currency for Frontend
     * @return string
     */
    public function getPriceFrontend()
    {
        return '<p class="price">' . $this->getPriceAsCurrency() . '</p>';
    }

    /**
     * Old Price As Currency for Frontend
     * @return string
     */
    public function getOldPriceFrontend()
    {
        return '<p class="old-price">' . $this->getOldPriceAsCurrency() . '</p>';
    }

    /**
     * Default Image Url
     * @return string
     */
    public function getDefaultImageUrl()
    {
        return isset($this->productImages[0]) ? $this->productImages[0]->path : '';
    }

    /**
     * Brands model to array
     * @return array
     */
    public static function getBrandsList()
    {
        return ArrayHelper::map(Brands::find()->where(['status' => Brands::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Groups model to array
     * @return array
     */
    public static function getGroupsList()
    {
        return ArrayHelper::map(Groups::find()->where(['status' => Groups::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Discounts model to array
     * @return array
     */
    public static function getDiscountsList()
    {
        return ArrayHelper::map(Discounts::find()->where(['status' => Discounts::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Gifts model to array
     * @param integer $id
     * @return array
     */
    public static function getGiftsList($id = 0)
    {
        return ($id == 0) ?
            ArrayHelper::map(Products::find()
                ->where(['!=', 'status', self::STATUS_DELETED])
                ->asArray()->all(), 'id', 'name') :
            ArrayHelper::map(Products::find()
                ->where(['!=', 'status', self::STATUS_DELETED])
                ->andWhere(['!=', 'id', $id])
                ->asArray()->all(), 'id', 'name');
    }

    /**
     * Ribbons model to array
     * @return array
     */
    public static function getRibbonsList()
    {
        return ArrayHelper::map(Ribbons::find()->where(['status' => Ribbons::STATUS_ACTIVE])->asArray()->all(), 'id', 'name');
    }

    /**
     * Categories model to array
     * @return array
     */
    public static function getCategoriesList()
    {
        return Categories::getCategoriesArray();
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->filter_ids = ArrayHelper::getColumn($this->productToFilters, 'filter_item_id');
        $this->ribbon_ids = ArrayHelper::getColumn($this->productToRibbons, 'ribbon_id');
        $this->category_ids = ArrayHelper::getColumn($this->productToCategories, 'category_id');
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->discount_id && empty($this->old_price)) {
                $this->old_price = $this->price;
                $this->price = ((100 - $this->discount->percentage) / 100) * $this->price;
            }

            if (empty($this->str_timer_start) || empty($this->str_timer_end)) {
                $this->timer_start = null;
                $this->timer_end = null;

            } else {
                $this->timer_start = strtotime($this->str_timer_start);
                $this->timer_end = strtotime($this->str_timer_end);
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($filter_ids = $this->filter_ids)) {
            if (!$insert)
                ProductToFilter::deleteAll(['product_id' => $this->id]);

            foreach ($filter_ids as $filter_id) {
                $relation = new ProductToFilter();
                $relation->product_id = $this->id;
                $relation->filter_item_id = $filter_id;
                $relation->save();
            }
        } else {
            if (!$insert)
                ProductToFilter::deleteAll(['product_id' => $this->id]);
        }

        if (!empty($ribbon_ids = $this->ribbon_ids)) {
            if (!$insert)
                ProductToRibbon::deleteAll(['product_id' => $this->id]);

            foreach ($ribbon_ids as $ribbon_id) {
                $relation = new ProductToRibbon();
                $relation->product_id = $this->id;
                $relation->ribbon_id = $ribbon_id;
                $relation->save();
            }
        } else {
            if (!$insert)
                ProductToRibbon::deleteAll(['product_id' => $this->id]);
        }

        if (!empty($category_ids = $this->category_ids)) {
            if (!$insert)
                ProductToCategory::deleteAll(['product_id' => $this->id]);

            foreach ($category_ids as $category_id) {
                $relation = new ProductToCategory();
                $relation->product_id = $this->id;
                $relation->category_id = $category_id;
                $relation->save();
            }
        }

        if (!empty($uploadedImages = json_decode($this->uploaded_images))) {
            if (empty($position = ProductImages::find()->where(['product_id' => $this->id])->max('position')))
                $position = 0;

            foreach ($uploadedImages as $file) {
                $image = new ProductImages();
                $image->product_id = $this->id;
                $image->generate_name = $file->generate_name;
                $image->name = $file->name;
                $image->path = $file->path;
                $image->position = $position;
                $image->save();
                $position++;
            }
        }

        if (!empty($deletedImages = json_decode($this->deleted_images))) {
            foreach ($deletedImages as $generate_name) {
                if (!empty($image = ProductImages::findOne(['generate_name' => $generate_name]))) {
                    $basePath = str_replace("backend", "", Yii::$app->basePath);
                    if ($image->delete()) unlink($basePath . $image->path);
                }
            }
        }

        if (!empty($sortedImages = json_decode($this->sorted_images))) {
            $position = 0;
            foreach ($sortedImages as $item) {
                if (!empty($image = ProductImages::findOne(['generate_name' => $item->key]))) {
                    $image->position = $position;
                    $image->save();
                    $position++;
                }
            }
        }
    }
}
