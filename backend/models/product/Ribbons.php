<?php

namespace backend\models\product;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_ribbons".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property string $image
 * @property string $link
 * @property string $description
 * @property int $position
 * @property int $weight
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ProductToRibbon[] $productToRibbons
 * @property Products[] $products
 */
class Ribbons extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_ribbons';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'name'], 'required'],
            [['description'], 'string'],
            [['position', 'weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['key', 'name', 'image', 'link'], 'string', 'max' => 255],
            [['key'], 'unique'],
            [['key'], 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u'],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['position'], 'default', 'value' => self::POSITION_TOP],
            [['weight'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'key' => Yii::t('model', 'Key'),
            'name' => Yii::t('model', 'Name'),
            'image' => Yii::t('model', 'Image'),
            'link' => Yii::t('model', 'Link'),
            'description' => Yii::t('model', 'Description'),
            'position' => Yii::t('model', 'Position'),
            'weight' => Yii::t('model', 'Weight'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToRibbons()
    {
        return $this->hasMany(ProductToRibbon::className(), ['ribbon_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])
            ->viaTable('product_to_ribbon', ['ribbon_id' => 'id']);
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Position
     */
    const POSITION_TOP = 0;
    const POSITION_BOTTOM = 1;
    const POSITION_RIGHT = 2;
    const POSITION_LEFT = 3;

    /**
     * Position List
     * @param integer|null $position
     * @return array
     */
    public static function getPositionArray($position = null)
    {
        $array = [
            self::POSITION_TOP => 'Top',
            self::POSITION_BOTTOM => 'Bottom',
            self::POSITION_RIGHT => 'Right',
            self::POSITION_LEFT => 'Left',
        ];

        return $position === null ? $array : $array[$position];
    }

    /**
     * Position Name
     * @return string
     */
    public function getPositionName()
    {
        return '<span class="text-bold text-blue">' . self::getPositionArray($this->position) . '</span>';
    }
}
