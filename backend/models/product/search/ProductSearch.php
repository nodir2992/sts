<?php

namespace backend\models\product\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\product\Products;

/**
 * ProductSearch represents the model behind the search form about `backend\models\product\Products`.
 */
class ProductSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'brand_id', 'group_id', 'discount_id', 'price', 'old_price', 'quantity', 'views', 'votes', 'status', 'ribbon_id', 'category_id'], 'integer'],
            [['barcode', 'slug', 'name', 'description', 'information', 'detail', 'meta_title', 'meta_keywords', 'meta_description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        if (isset($params['per_page']))
            $dataProvider->pagination->pageSize = $params['per_page'];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'group_id' => $this->group_id,
            'discount_id' => $this->discount_id,
            'price' => $this->price,
            'old_price' => $this->old_price,
            'quantity' => $this->quantity,
            'views' => $this->views,
            'votes' => $this->votes,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'detail', $this->detail])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        if ($this->ribbon_id) {
            $query->leftJoin('product_to_ribbon', 'product_to_ribbon.product_id=products.id')
                ->andFilterWhere(['product_to_ribbon.ribbon_id' => $this->ribbon_id])
                ->groupBy('product_to_ribbon.product_id');
        }

        if ($this->category_id) {
            $query->leftJoin('product_to_category', 'product_to_category.product_id=products.id')
                ->andFilterWhere(['product_to_category.category_id' => $this->category_id])
                ->groupBy('product_to_category.product_id');
        }

        if (!is_null($this->created_at) && strpos($this->created_at, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);
            $query->andFilterWhere(['between', 'created_at', strtotime($start_date), (strtotime($end_date) + 86400)]);
        }

        if (!is_null($this->updated_at) && strpos($this->updated_at, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->updated_at);
            $query->andFilterWhere(['between', 'updated_at', strtotime($start_date), (strtotime($end_date) + 86400)]);
        }

        $query->andFilterWhere(['!=', 'status', self::STATUS_DELETED]);

        return $dataProvider;
    }
}
