<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this  yii\web\View */
/* @var $model backend\modules\admin\models\BizRule */
/* @var $form ActiveForm */
?>

<div class="auth-item-form box-body">
    <div class="row">
        <div class="col-lg-4 col-sm-8">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

            <?= $form->field($model, 'className')->textInput() ?>

            <div class="form-group">
                <?php
                echo Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin', 'Update'), [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>