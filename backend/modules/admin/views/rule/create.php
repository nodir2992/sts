<?php

/* @var $this  yii\web\View */
/* @var $model backend\modules\admin\models\BizRule */

$this->title = Yii::t('rbac-admin', 'Create Rule');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create box box-success">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

</div>
