<?php

/* @var $this yii\web\View */
/* @var $model backend\models\location\Locations */
/* @var $modelItems backend\models\location\Locations */

$this->title = Yii::t('views', 'Update Location: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('views', 'Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('views', 'Update');
?>
<div class="locations-update box box-primary">

    <?= $this->render('_form', [
        'model' => $model,
        'modelItems' => $modelItems,
    ]) ?>

</div>
