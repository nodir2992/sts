<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\touchspin\TouchSpin;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\order\Orders */
/* @var $modelItem backend\models\order\OrderItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form box-body">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'order_form']]); ?>

    <div class="row">
        <div class="col-lg-6 col-md-8">

            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',
                'widgetBody' => '.container-orders',
                'widgetItem' => '.order-item',
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $model->orderItems[0],
                'formId' => 'order_form',
                'formFields' => [
                    'quantity'
                ],
            ]); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-cubes"></i> <strong><?= Yii::t('views', 'Order Items') ?></strong>
                </div>
                <div class="panel-body container-orders">
                    <?php
                    foreach ($model->orderItems as $index => $modelItem):
                        $minQty = 1;
                        $maxQty = $modelItem->product->quantity ?: 1000; ?>
                        <div class="order-item panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-cube"></i> <strong class="panel-title-order">
                                    <?= $modelItem->product->name ?></strong>
                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs">
                                    <i class="fa fa-minus"></i></button>
                            </div>
                            <div class="panel-body">
                                <?= Html::activeHiddenInput($modelItem, "[{$index}]product_id") ?>
                                <div class="row">
                                    <div class="col-sm-6" align="center">
                                        <?= Html::img($modelItem->product->getDefaultImageUrl(), ['alt' => 'product-image', 'width' => '125px']) ?>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <?= $form->field($modelItem, "[{$index}]price") ?>
                                        <?= $form->field($modelItem, "[{$index}]quantity")->widget(TouchSpin::classname(), [
                                            'pluginOptions' => [
                                                'min' => $minQty,
                                                'max' => $maxQty,
                                                'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                                                'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                                                'buttonup_class' => $modelItem->quantity == $maxQty ? 'disabled btn btn-default' : 'btn btn-default',
                                                'buttondown_class' => $modelItem->quantity == $minQty ? 'disabled btn btn-default' : 'btn btn-default',
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'sendEmail')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('views', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
