<?php

use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\widgets\PageSize;
use backend\models\order\Orders;
use kartik\daterange\DateRangePicker;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\order\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('views', 'Orders');
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('.kv-drp-dropdown { white-space: nowrap; display: flex !important; }');
?>

<div class="orders-index box box-default">
    <div class="box-body">
        <div class="table-responsive">
            <?php Pjax::begin(); ?>
            <?= PageSize::widget() ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="per_page"]',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'id',
                        'options' => ['width' => '70'],
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'filter' => Orders::getStatusArray(),
                        'value' => function (Orders $model) {
                            return $model->getStatusName();
                        },
                    ],
                    [
                        'attribute' => 'payment',
                        'format' => 'raw',
                        'filter' => Orders::getPaymentArray(),
                        'value' => function (Orders $model) {
                            return $model->getPaymentName();
                        },
                    ],
                    [
                        'attribute' => 'user_id',
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'user_id',
                            'data' => Orders::getUsersList(),
                            'theme' => Select2::THEME_DEFAULT,
                            'options' => ['placeholder' => ''],
                            'pluginOptions' => ['allowClear' => true],
                        ]),
                        'options' => ['width' => '200'],
                        'value' => function (Orders $model) {
                            return ($model->user_id !== 0) ? $model->customer->username : 'Гость';
                        }
                    ],
                    'first_name',
                    'last_name',
                    'phone',
                    [
                        'attribute' => 'location_id',
                        'filter' => Orders::getLocationsList(),
                        'value' => function (Orders $model) {
                            return $model->location->name;
                        },
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['datetime'],
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'convertFormat' => true,
                            'presetDropdown' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd.m.Y'
                                ],
                                'opens' => 'left'
                            ],
                            'pluginEvents' => [
                                'cancel.daterangepicker' => 'function(event) {
                                    $("#ordersearch-created_at").val("");
                                    $(".grid-view").yiiGridView("applyFilter");
                                 }',
                            ]
                        ]),
                    ],
                    [
                        'attribute' => 'total_price',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                        'value' => function (Orders $model) {
                            return Yii::$app->formatter->asDecimal(array_sum(array_map(function ($item) {
                                    return $item['total_price'];
                                }, $model->orderItems)), 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>');
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => Helper::filterActionColumn(['view', 'delete'])
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
