<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use backend\widgets\PageSize;
use mihaildev\elfinder\InputFile;
use backend\widgets\ActionsApply;
use backend\models\product\Products;
use kartik\daterange\DateRangePicker;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\product\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $allColumns array */
/* @var $visibleColumns array */

$this->title = Yii::t('views', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="products-index box box-default">
    <div class="box-body">
        <p>
            <?php
            if (Helper::checkRoute('create'))
                echo Html::a(Yii::t('views', 'Create'), ['create'], ['class' => 'btn btn-success']) . '&nbsp;';

            if (Helper::checkRoute('dollar-rate'))
                echo Html::a(Yii::t('views', 'Dollar Rate'), ['dollar-rate'], ['class' => 'btn btn-info']);
            ?>
            <span class="btn-group">
                <?php
                if (Helper::checkRoute('excel-export'))
                    echo Html::a(Yii::t('views', 'Excel export'), ['excel-export'], ['class' => 'btn btn-warning']);

                if (Helper::checkRoute('excel-import')) {
                    Modal::begin([
                        'header' => '<h4>' . Yii::t('views', 'Excel import') . '</h4>',
                        'toggleButton' => [
                            'label' => Yii::t('views', 'Excel import'),
                            'class' => 'btn btn-warning'
                        ],
                    ]);
                    echo Html::beginForm(['excel-import'], 'POST')
                        . InputFile::widget([
                            'controller' => 'elfinder',
                            'path' => '/product/excel',
                            'name' => 'file',
                            'template' => '<div class="input-group">{input}<div class="input-group-btn">{button}</div>',
                            'options' => ['class' => 'form-control', 'required' => true],
                            'buttonOptions' => ['class' => 'btn btn-primary'],
                            'buttonName' => Yii::t('views', 'Select file'),
                            'multiple' => false
                        ])
                        . '<div class="input-group-btn">'
                        . Html::submitButton(Yii::t('views', 'Send'), ['class' => 'btn btn-success'])
                        . '</div></div>'
                        . Html::endForm();
                    Modal::end();
                } ?>
            </span>
        </p>

        <div class="table-responsive">
            <?php
            Pjax::begin();

            echo Html::beginForm(Url::current(['visibleColumns' => null]), 'get',
                    ['data-pjax' => true, 'class' => 'form-inline margin-bottom-10'])
                . Html::checkboxList('visibleColumns', $visibleColumns, $allColumns, ['class' => 'form-group'])
                . '&nbsp;&nbsp;'
                . '<span class="btn-group">'
                . Html::submitButton(Yii::t('views', 'Apply'), ['class' => 'btn btn-primary'])
                . Html::a(Yii::t('views', 'Reset'), ['product/index'], ['class' => 'btn btn-danger'])
                . '</span>'
                . Html::endForm();

            ActionsApply::begin([
                'actions' => [
                    'deactivate' => Yii::t('views', 'Inactive'),
                    'available' => Yii::t('views', 'Available'),
                    'not_available' => Yii::t('views', 'Not available'),
                    'delete' => Yii::t('views', 'Delete'),
                ],
                'template' => '<div class="form-inline margin-bottom-5">{list}{button}'
            ]);

            echo PageSize::widget([
                    'template' => Helper::checkRoute('apply-items') ? '{list}&nbsp;{label}</div>' :
                        '<div class="form-inline margin-bottom-5">{list}&nbsp;{label}</div>'
                ])
                . GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => 'select[name="per_page"]',
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['class' => 'yii\grid\CheckboxColumn'],

                        [
                            'attribute' => 'id',
                            'options' => ['width' => '60'],
                            'visible' => in_array('id', $visibleColumns),
                        ],
                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'options' => ['width' => '240'],
                            'value' => function (Products $data) {
                                return (Helper::checkRoute('view')) ?
                                    Html::a($data->name, ['view', 'id' => $data->id], ['data-pjax' => 0]) :
                                    $data->name;
                            },
                            'visible' => in_array('name', $visibleColumns),
                        ],
                        [
                            'attribute' => 'barcode',
                            'visible' => in_array('barcode', $visibleColumns),
                        ],
                        [
                            'attribute' => 'price',
                            'format' => 'raw',
                            'options' => ['width' => '140'],
                            'value' => function (Products $data) {
                                return $data->price . ' $<br>' . $data->getPriceAsCurrency();
                            },
                            'visible' => in_array('price', $visibleColumns),
                        ],
                        [
                            'attribute' => 'old_price',
                            'format' => 'raw',
                            'options' => ['width' => '140'],
                            'value' => function (Products $data) {
                                return $data->old_price ? $data->old_price . ' $<br>' . $data->getOldPriceAsCurrency() : $data->old_price;
                            },
                            'visible' => in_array('old_price', $visibleColumns),
                        ],
                        [
                            'attribute' => 'brand_id',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'brand_id',
                                'data' => Products::getBrandsList(),
                                'theme' => Select2::THEME_DEFAULT,
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]),
                            'options' => ['width' => '140'],
                            'value' => function (Products $data) {
                                return Html::a($data->brand->name, ['brand/view', 'id' => $data->brand_id],
                                    ['target' => '_blank', 'data-pjax' => 0]);
                            },
                            'visible' => in_array('brand', $visibleColumns),
                        ],
                        [
                            'attribute' => 'group_id',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'group_id',
                                'data' => Products::getGroupsList(),
                                'theme' => Select2::THEME_DEFAULT,
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]),
                            'options' => ['width' => '180'],
                            'value' => function (Products $data) {
                                return Html::a($data->group->name, ['group/view', 'id' => $data->group_id],
                                    ['target' => '_blank', 'data-pjax' => 0]);
                            },
                            'visible' => in_array('group', $visibleColumns),
                        ],
                        [
                            'attribute' => 'discount_id',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'discount_id',
                                'data' => Products::getDiscountsList(),
                                'theme' => Select2::THEME_DEFAULT,
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]),
                            'options' => ['width' => '140'],
                            'value' => function (Products $data) {
                                return $data->discount_id ? Html::a($data->discount->name, ['discount/view', 'id' => $data->discount_id],
                                    ['target' => '_blank', 'data-pjax' => 0]) : $data->discount_id;
                            },
                            'visible' => in_array('discount', $visibleColumns),
                        ],
                        [
                            'attribute' => 'ribbon_id',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'ribbon_id',
                                'data' => Products::getRibbonsList(),
                                'theme' => Select2::THEME_DEFAULT,
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]),
                            'options' => ['width' => '160'],
                            'value' => function (Products $data) {
                                $result = '';
                                if (!empty($ribbons = $data->ribbons)) {
                                    foreach ($ribbons as $ribbon) {
                                        $result .= Html::a($ribbon->name, ['ribbon/view', 'id' => $ribbon->id],
                                                ['target' => '_blank', 'data-pjax' => 0]) . '<br>';
                                    }
                                }
                                return $result;
                            },
                            'visible' => in_array('ribbon', $visibleColumns),
                        ],
                        [
                            'attribute' => 'category_id',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'category_id',
                                'data' => Products::getCategoriesList(),
                                'theme' => Select2::THEME_DEFAULT,
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]),
                            'options' => ['width' => '200'],
                            'value' => function (Products $data) {
                                $result = '';
                                if (!empty($categories = $data->categories)) {
                                    foreach ($categories as $category) {
                                        $result .= Html::a($category->name, ['category/view', 'id' => $category->id],
                                                ['target' => '_blank', 'data-pjax' => 0]) . '<br>';
                                    }
                                }
                                return $result;
                            },
                            'visible' => in_array('category', $visibleColumns),
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'status',
                                'data' => Products::getStatusArray(),
                                'theme' => Select2::THEME_DEFAULT,
                                'options' => ['placeholder' => ''],
                                'hideSearch' => true,
                                'pluginOptions' => ['allowClear' => true],
                            ]),
                            'options' => ['width' => '140'],
                            'value' => function (Products $data) {
                                return $data->getStatusName();
                            },
                            'visible' => in_array('status', $visibleColumns),
                        ],
                        [
                            'attribute' => 'gift_id',
                            'format' => 'raw',
                            'options' => ['width' => '140'],
                            'value' => function (Products $data) {
                                return $data->gift_id ? Html::a($data->gift->name, ['view', 'id' => $data->gift_id],
                                    ['target' => '_blank', 'data-pjax' => 0]) : $data->gift_id;
                            },
                            'visible' => in_array('gift', $visibleColumns),
                        ],
                        [
                            'attribute' => 'created_at',
                            'format' => ['datetime'],
                            'filter' => DateRangePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_at',
                                'convertFormat' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd.m.Y'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    'cancel.daterangepicker' => 'function(event) {
                                        $("#productsearch-created_at").val("");
                                        $(".grid-view").yiiGridView("applyFilter");
                                    }',
                                ]
                            ]),
                            'visible' => in_array('created_at', $visibleColumns),
                        ],
                        [
                            'attribute' => 'updated_at',
                            'format' => ['datetime'],
                            'filter' => DateRangePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'updated_at',
                                'convertFormat' => true,
                                'pluginOptions' => [
                                    'locale' => [
                                        'format' => 'd.m.Y'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    'cancel.daterangepicker' => 'function(event) {
                                        $("#productsearch-updated_at").val("");
                                        $(".grid-view").yiiGridView("applyFilter");
                                     }',
                                ]
                            ]),
                            'visible' => in_array('updated_at', $visibleColumns),
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => Helper::filterActionColumn(['view', 'update', 'delete'])
                        ],
                    ],
                ]);
            ActionsApply::end();
            Pjax::end(); ?>
        </div>
    </div>
</div>
