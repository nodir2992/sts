<?php

use yii\helpers\Html;
use backend\models\order\Orders;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $dataByStatus array */
/* @var $dataByPayment array */
/* @var $dataDaily array */
/* @var $dataFilterStatus array */
/* @var $dataFilterPayment array */
/* @var $filterInputValue string */
/* @var $filterInputValueDefault string */
/* @var $topClients array */
/* @var $bestsellers array */

$this->title = Yii::t('views', 'Отчет о продажах');
$this->registerCss('.table > thead > tr > th{ text-align: left; }');
?>

<div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('views', 'По дата') ?></h3>
            </div>
            <div class="ms-orders-index box-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= Yii::t('views', 'Дата') ?></th>
                            <th><?= Yii::t('views', 'Количество') ?></th>
                            <th><?= Yii::t('views', 'Общая сумма') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($dataDaily['today']) {
                            $totalPrice = $dataDaily['today']['totalPrice'] ?: 0;
                            echo '<tr><td><strong>'
                                . Yii::t('views', 'Сегодня')
                                . '</strong></td><td>'
                                . $dataDaily['today']['quantity']
                                . '</td><td>'
                                . Yii::$app->formatter->asDecimal($totalPrice, 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>')
                                . '</td></tr>';
                        }
                        if ($dataDaily['yesterday']) {
                            $totalPrice = $dataDaily['yesterday']['totalPrice'] ?: 0;
                            echo '<tr><td><strong>'
                                . Yii::t('views', 'Вчера')
                                . '</strong></td><td>'
                                . $dataDaily['yesterday']['quantity']
                                . '</td><td>'
                                . Yii::$app->formatter->asDecimal($totalPrice, 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>')
                                . '</td></tr>';
                        }
                        if ($dataDaily['week']) {
                            $totalPrice = $dataDaily['week']['totalPrice'] ?: 0;
                            echo '<tr><td><strong>'
                                . Yii::t('views', 'Неделя')
                                . '</strong></td><td>'
                                . $dataDaily['week']['quantity']
                                . '</td><td>'
                                . Yii::$app->formatter->asDecimal($totalPrice, 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>')
                                . '</td></tr>';
                        }
                        if ($dataDaily['month']) {
                            $totalPrice = $dataDaily['month']['totalPrice'] ?: 0;
                            echo '<tr><td><strong>'
                                . Yii::t('views', 'Месяц')
                                . '</strong></td><td>'
                                . $dataDaily['month']['quantity']
                                . '</td><td>'
                                . Yii::$app->formatter->asDecimal($totalPrice, 0)
                                . Yii::t('model', '&nbsp;<i>сум</i>')
                                . '</td></tr>';
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('views', 'По статусу') ?></h3>
            </div>
            <div class="ms-orders-index box-body">
                <?php if ($dataByStatus) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= Yii::t('views', 'Статус') ?></th>
                                <th><?= Yii::t('views', 'Количество') ?></th>
                                <th><?= Yii::t('views', 'Общая сумма') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($dataByStatus as $item) {
                                echo '<tr><td><strong>'
                                    . Orders::getStatusArray($item['status'])
                                    . '</strong></td><td>'
                                    . $item['quantity']
                                    . '</td><td>'
                                    . Yii::$app->formatter->asDecimal($item['totalPrice'], 0)
                                    . Yii::t('model', '&nbsp;<i>сум</i>')
                                    . '</td></tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    echo '<p>' . Yii::t('views', 'No results found.') . '</p>';
                } ?>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('views', 'По типу оплаты') ?></h3>
            </div>
            <div class="ms-orders-index box-body">
                <?php if ($dataByPayment) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= Yii::t('views', 'Тип оплаты') ?></th>
                                <th><?= Yii::t('views', 'Количество') ?></th>
                                <th><?= Yii::t('views', 'Общая сумма') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($dataByPayment as $item) {
                                echo '<tr><td><strong>'
                                    . Orders::getPaymentArray($item['payment'])
                                    . '</strong></td><td>'
                                    . $item['quantity']
                                    . '</td><td>'
                                    . Yii::$app->formatter->asDecimal($item['totalPrice'], 0)
                                    . Yii::t('model', '&nbsp;<i>сум</i>')
                                    . '</td></tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    echo '<p>' . Yii::t('views', 'No results found.') . '</p>';
                } ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('views', 'По фильтру') ?></h3>
            </div>
            <div class="ms-orders-index box-body">
                <?php
                echo Html::beginForm(['index'], 'GET', ['id' => 'filterDateForm'])
                    . DateRangePicker::widget([
                        'name' => 'filter_date',
                        'value' => $filterInputValue,
                        'id' => 'filterDateInput',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'd.m.Y'
                            ],
                            'opens' => 'left'
                        ],
                        'pluginEvents' => [
                            'cancel.daterangepicker' => 'function(event) {
                                $("#filterDateInput").val($("#defaultValue").val());
                                $("#filterDateForm").submit();
                            }',
                            'apply.daterangepicker' => 'function(event) {
                                $("#filterDateForm").submit();
                            }',
                        ]
                    ])
                    . Html::hiddenInput('default_value', $filterInputValueDefault, ['id' => 'defaultValue'])
                    . Html::endForm(); ?>

                <hr>
                <h4><?= Yii::t('views', 'По статусу') ?></h4>
                <hr>
                <?php if ($dataFilterStatus) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= Yii::t('views', 'Статус') ?></th>
                                <th><?= Yii::t('views', 'Количество') ?></th>
                                <th><?= Yii::t('views', 'Общая сумма') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($dataFilterStatus as $item) {
                                echo '<tr><td><strong>'
                                    . Orders::getStatusArray($item['status'])
                                    . '</strong></td><td>'
                                    . $item['quantity']
                                    . '</td><td>'
                                    . Yii::$app->formatter->asDecimal($item['totalPrice'], 0)
                                    . Yii::t('model', '&nbsp;<i>сум</i>')
                                    . '</td></tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    echo '<p>' . Yii::t('views', 'No results found.') . '</p>';
                } ?>

                <hr>
                <h4><?= Yii::t('views', 'По типу оплаты') ?></h4>
                <hr>
                <?php if ($dataFilterPayment) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= Yii::t('views', 'Тип оплаты') ?></th>
                                <th><?= Yii::t('views', 'Количество') ?></th>
                                <th><?= Yii::t('views', 'Общая сумма') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($dataFilterPayment as $item) {
                                echo '<tr><td><strong>'
                                    . Orders::getPaymentArray($item['payment'])
                                    . '</strong></td><td>'
                                    . $item['quantity']
                                    . '</td><td>'
                                    . Yii::$app->formatter->asDecimal($item['totalPrice'], 0)
                                    . Yii::t('model', '&nbsp;<i>сум</i>')
                                    . '</td></tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    echo '<p>' . Yii::t('views', 'No results found.') . '</p>';
                } ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('views', 'Топ покупателей') ?></h3>
            </div>
            <div class="ms-orders-index box-body">
                <?php if ($topClients) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= Yii::t('views', 'Покупатель') ?></th>
                                <th><?= Yii::t('views', 'Количество заказов') ?></th>
                                <th><?= Yii::t('views', 'Общая сумма заказов') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($topClients as $client) {
                                echo '<tr><td>'
//                                    . Html::a($client['email'], ['#'], ['target' => '_blank'])
                                    . $client['email']
                                    . '</td><td>'
                                    . $client['qty']
                                    . '</td><td>'
                                    . Yii::$app->formatter->asDecimal($client['total'], 0)
                                    . Yii::t('model', '&nbsp;<i>сум</i>')
                                    . '</td></tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    echo '<p>' . Yii::t('views', 'No results found.') . '</p>';
                } ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('views', 'Бестселлеры') ?></h3>
            </div>
            <div class="ms-orders-index box-body">
                <?php if ($bestsellers) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= Yii::t('views', 'Товар') ?></th>
                                <th><?= Yii::t('views', 'Количество') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($bestsellers as $product) {
                                echo '<tr><td>'
                                    . Html::a($product['name'], ['product/view', 'id' => $product['id']], ['target' => '_blank'])
                                    . '</td><td>'
                                    . $product['qty']
                                    . '</td></tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    echo '<p>' . Yii::t('views', 'No results found.') . '</p>';
                } ?>
            </div>
        </div>
    </div>
</div>
