<?php
return [
    'adminEmail' => 'nodir.madiyarov@gmail.com',
    'noreplyEmail' => 'noreply@sts-hik.uz',
    'infoEmail' => 'info@sts-hik.uz',
    'bccEmail' => 'bcc@sts-hik.uz',
    'supportEmail' => 'support@sts-hik.uz',
    'user.passwordResetTokenExpire' => 3600,
];
