<?php

use yii\db\Migration;
use backend\models\page\Pages;
use backend\models\menu\MenuItems;

class m170418_104813_pages extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Pages::tableName(), [
            'id' => $this->primaryKey(),
            'url' => $this->string(32)->notNull()->unique(),
            'name' => $this->string(64)->notNull(),
            'body' => $this->text()->null(),
            'meta_title' => $this->string(64)->notNull(),
            'meta_keywords' => $this->string()->null(),
            'meta_description' => $this->text()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(Pages::STATUS_INACTIVE),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert(MenuItems::tableName(), [
            'id' => 17,
            'menu_id' => 1,
            'parent_id' => 14,
            'label' => 'Pages',
            'url' => '/pages/index',
            'class' => 'fa fa-list-alt',
            'icon' => '',
            'description' => '',
            'weight' => 0,
            'status' => MenuItems::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    public function down()
    {
        $this->delete(MenuItems::tableName(), ['id' => 17]);
        $this->dropTable(Pages::tableName());
    }
}
