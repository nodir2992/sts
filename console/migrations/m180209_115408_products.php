<?php

use yii\db\Migration;

/**
 * Class m180209_115408_products
 */
class m180209_115408_products extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('product_dollar_rate', [
            'id' => $this->primaryKey(),
            'value' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_brands', [
            'id' => $this->primaryKey(),
            'slug' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'link' => $this->string()->null(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_ribbons', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'link' => $this->string()->null(),
            'description' => $this->text()->null(),
            'position' => $this->smallInteger()->notNull()->defaultValue(0),
            'weight' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_discounts', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'percentage' => $this->integer()->notNull(),
            'image' => $this->string()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_categories', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->null(),
            'slug' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'description' => $this->text()->null(),
            'meta_title' => $this->string()->notNull(),
            'meta_keywords' => $this->text()->null(),
            'meta_description' => $this->text()->null(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_filters', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_filter_items', [
            'id' => $this->primaryKey(),
            'filter_id' => $this->integer()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
            'discount_id' => $this->integer()->null(),
            'barcode' => $this->string(64)->notNull()->unique(),
            'slug' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->null(),
            'information' => $this->text()->null(),
            'detail' => $this->text()->null(),
            'meta_title' => $this->string()->notNull(),
            'meta_keywords' => $this->text()->null(),
            'meta_description' => $this->text()->null(),
            'price' => $this->money(10, 2)->notNull(),
            'old_price' => $this->money(10, 2)->null(),
            'quantity' => $this->integer()->null(),
            'timer_start' => $this->integer()->null(),
            'timer_end' => $this->integer()->null(),
            'views' => $this->integer()->null(),
            'votes' => $this->integer()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_images', [
            'product_id' => $this->integer()->notNull(),
            'generate_name' => $this->string(64)->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'path' => $this->string()->notNull(),
            'position' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('product_to_category', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_to_ribbon', [
            'product_id' => $this->integer()->notNull(),
            'ribbon_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_to_filter', [
            'product_id' => $this->integer()->notNull(),
            'filter_item_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-products-brand_id', 'products', 'brand_id', 'product_brands', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk-products-group_id', 'products', 'group_id', 'product_groups', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk-products-discount_id', 'products', 'discount_id', 'product_discounts', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk-product_filters-group_id', 'product_filters', 'group_id', 'product_groups', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk-product_filter_items-filter_id', 'product_filter_items', 'filter_id', 'product_filters', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk-categories-parent_id', 'product_categories', 'parent_id', 'product_categories', 'id', 'CASCADE', 'RESTRICT');

        $this->addPrimaryKey('pk-product_images', 'product_images', ['product_id', 'generate_name']);
        $this->createIndex('idx-product_images-product_id', 'product_images', 'product_id');
        $this->createIndex('idx-product_images-generate_name', 'product_images', 'generate_name');
        $this->addForeignKey('fk-product_images-product_id', 'product_images', 'product_id', 'products', 'id', 'CASCADE', 'RESTRICT');

        $this->addPrimaryKey('pk-product_to_category', 'product_to_category', ['product_id', 'category_id']);
        $this->createIndex('idx-product_to_category-product_id', 'product_to_category', 'product_id');
        $this->createIndex('idx-product_to_category-category_id', 'product_to_category', 'category_id');
        $this->addForeignKey('fk-product_to_category-product_id', 'product_to_category', 'product_id', 'products', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-product_to_category-category_id', 'product_to_category', 'category_id', 'product_categories', 'id', 'CASCADE', 'RESTRICT');

        $this->addPrimaryKey('pk-product_to_ribbon', 'product_to_ribbon', ['product_id', 'ribbon_id']);
        $this->createIndex('idx-product_to_ribbon-product_id', 'product_to_ribbon', 'product_id');
        $this->createIndex('idx-product_to_ribbon-ribbon_id', 'product_to_ribbon', 'ribbon_id');
        $this->addForeignKey('fk-product_to_ribbon-product_id', 'product_to_ribbon', 'product_id', 'products', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-product_to_ribbon-ribbon_id', 'product_to_ribbon', 'ribbon_id', 'product_ribbons', 'id', 'CASCADE', 'RESTRICT');

        $this->addPrimaryKey('pk-product_to_filter', 'product_to_filter', ['product_id', 'filter_item_id']);
        $this->createIndex('idx-product_to_filter-product_id', 'product_to_filter', 'product_id');
        $this->createIndex('idx-product_to_filter-filter_item_id', 'product_to_filter', 'filter_item_id');
        $this->addForeignKey('fk-product_to_filter-product_id', 'product_to_filter', 'product_id', 'products', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-product_to_filter-filter_item_id', 'product_to_filter', 'filter_item_id', 'product_filter_items', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('product_to_filter');
        $this->dropTable('product_to_ribbon');
        $this->dropTable('product_to_category');
        $this->dropTable('product_images');
        $this->dropTable('products');
        $this->dropTable('product_filter_items');
        $this->dropTable('product_filters');
        $this->dropTable('product_categories');
        $this->dropTable('product_discounts');
        $this->dropTable('product_ribbons');
        $this->dropTable('product_groups');
        $this->dropTable('product_brands');
        $this->dropTable('product_dollar_rate');
    }
}
