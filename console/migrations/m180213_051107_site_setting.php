<?php

use yii\db\Migration;

/**
 * Class m180213_051107_site_setting
 */
class m180213_051107_site_setting extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string(64)->notNull()->unique(),
            'label' => $this->string(64)->notNull(),
            'value' => $this->text()->null(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0),
            'required' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('settings');
    }
}
