<?php

use yii\db\Migration;

/**
 * Class m180213_062039_posts
 */
class m180213_062039_posts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('post_categories', [
            'id' => $this->primaryKey(),
            'slug' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'description' => $this->text()->null(),
            'meta_title' => $this->string()->notNull(),
            'meta_keywords' => $this->string()->null(),
            'meta_description' => $this->text()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'title' => $this->string()->notNull(),
            'image' => $this->string()->notNull(),
            'summary' => $this->text()->null(),
            'body' => $this->text()->null(),
            'meta_title' => $this->string()->notNull(),
            'meta_keywords' => $this->string()->null(),
            'meta_description' => $this->text()->null(),
            'views' => $this->integer()->null()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product_to_post', [
            'product_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-posts-category_id', 'posts', 'category_id', 'post_categories', 'id',
            'CASCADE', 'NO ACTION');

        $this->addPrimaryKey('pk-product_to_post', 'product_to_post', ['product_id', 'post_id']);
        $this->createIndex('idx-product_to_post-product_id', 'product_to_post', 'product_id');
        $this->createIndex('idx-product_to_post-post_id', 'product_to_post', 'post_id');
        $this->addForeignKey('fk-product_to_post-product_id', 'product_to_post', 'product_id', 'products', 'id',
            'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-product_to_post-post_id', 'product_to_post', 'post_id', 'posts', 'id',
            'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('product_to_post');
        $this->dropTable('posts');
        $this->dropTable('post_categories');
    }
}
