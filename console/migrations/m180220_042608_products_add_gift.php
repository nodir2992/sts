<?php

use yii\db\Migration;

/**
 * Class m180220_042608_products_add_gift
 */
class m180220_042608_products_add_gift extends Migration
{
    public function up()
    {
        $this->addColumn('products', 'gift_id', $this->integer()->null()->after('discount_id'));
        $this->addForeignKey('fk-products-gift_id', 'products', 'gift_id', 'products', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('fk-products-gift_id', 'products');
        $this->dropColumn('products', 'gift_id');
    }
}
