<?php

use yii\db\Migration;

/**
 * Class m180220_061641_locations
 */
class m180220_061641_locations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('locations', [
            'id' => $this->primaryKey(),
            'location_id' => $this->integer()->null(),
            'type' => $this->char(16)->notNull(),
            'name' => $this->string()->notNull(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-locations', 'locations', 'type');
        $this->addForeignKey('fk-locations-location_id', 'locations', 'location_id', 'locations', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('locations');
    }
}
