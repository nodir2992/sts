<?php

use yii\db\Migration;

/**
 * Class m180220_100350_user_add_columns
 */
class m180220_100350_user_add_columns extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'phone', $this->string()->after('image'));
        $this->addColumn('user', 'location_id', $this->integer()->after('phone'));
        $this->addColumn('user', 'address', $this->string()->after('location_id'));
    }

    public function down()
    {
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'location_id');
        $this->dropColumn('user', 'address');
    }
}
