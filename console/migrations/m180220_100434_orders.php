<?php

use yii\db\Migration;

/**
 * Class m180220_100434_orders
 */
class m180220_100434_orders extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('basket', [
            'user_id' => $this->integer()->notNull()->defaultValue(0),
            'session_id' => $this->string()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->defaultValue(0),
            'session_id' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'location_id' => $this->integer()->notNull(),
            'address' => $this->string()->notNull(),
            'comment' => $this->text()->null(),
            'payment' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('order_items', [
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'gift_id' => $this->integer()->null(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'total_price' => $this->integer()->null(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-basket', 'basket', ['user_id', 'session_id', 'product_id']);
        $this->addPrimaryKey('pk-order_items', 'order_items', ['order_id', 'product_id']);
        $this->addForeignKey('fk-order_items-order_id', 'order_items', 'order_id', 'orders', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('order_items');
        $this->dropTable('orders');
        $this->dropTable('basket');
    }
}
