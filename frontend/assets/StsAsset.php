<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * SmartAsset bundle for the Theme Smart css and js files.
 */
class StsAsset extends AssetBundle
{
//    public $basePath = '@webroot/sts';
//    public $baseUrl = '@web/sts';
    public $sourcePath = '@webroot/sts';
    public $css = [
        'css/main.css',
    ];
    public $js = [
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
