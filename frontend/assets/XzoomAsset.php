<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * SmartAsset bundle for the Theme Smart css and js files.
 */
class XzoomAsset extends AssetBundle
{
//    public $basePath = '@webroot/xzoom';
//    public $baseUrl = '@web/xzoom';
    public $sourcePath = '@webroot/xzoom';
    public $css = [
        'css/xzoom.css',
        'magnific-popup/css/magnific-popup.css',
    ];
    public $js = [
        'hammer/jquery.hammer.min.js',
        'magnific-popup/js/magnific-popup.js',
        'js/xzoom.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
