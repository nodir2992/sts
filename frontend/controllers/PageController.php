<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Settings;
use backend\models\page\Pages;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * Displays contact page.
     * @return mixed
     */
    public function actionContact()
    {
        $modelForm = new ContactForm();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            if ($modelForm->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Благодарим Вас за обращение к нам. Мы ответим вам как можно скорее.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'При отправке вашего сообщения произошла ошибка.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'modelForm' => $modelForm,
                'model' => $this->findPage('contact'),
            ]);
        }
    }

    /**
     * Displays Service page.
     * @return mixed
     */
    public function actionService()
    {
        return $this->render('view', [
            'model' => $this->findPage('service'),
        ]);
    }

    /**
     * Displays Payment page.
     * @return mixed
     */
    public function actionPayment()
    {
        return $this->render('view', [
            'model' => $this->findPage('payment'),
        ]);
    }

    /**
     * Displays Delivery page.
     * @return mixed
     */
    public function actionDelivery()
    {
        return $this->render('view', [
            'model' => $this->findPage('delivery'),
        ]);
    }

    /**
     * Displays Stores page.
     * @return mixed
     */
    public function actionStores()
    {
        return $this->render('view', [
            'model' => $this->findPage('stores'),
        ]);
    }

    /**
     * Displays Guarantee page.
     * @return mixed
     */
    public function actionGuarantee()
    {
        return $this->render('view', [
            'model' => $this->findPage('guarantee'),
        ]);
    }

    /**
     * Displays Refund page.
     * @return mixed
     */
    public function actionRefund()
    {
        return $this->render('view', [
            'model' => $this->findPage('refund'),
        ]);
    }

    /**
     * Displays Feedback page.
     * @return mixed
     */
    public function actionFeedback()
    {
        $modelForm = new ContactForm();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            if ($modelForm->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Благодарим Вас за обращение к нам. Мы ответим вам как можно скорее.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'При отправке вашего сообщения произошла ошибка.'));
            }

            return $this->refresh();
        } else {
            return $this->render('feedback', [
                'modelForm' => $modelForm,
                'model' => $this->findPage('feedback'),
            ]);
        }
    }

    /**
     * Find Pages model.
     * @param string $url
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPage($url)
    {
        if (($model = Pages::findOne(['url' => $url, 'status' => Pages::STATUS_ACTIVE])) !== null) {
            $model->siteLogo = Yii::$app->request->hostInfo . Settings::getSettingValue('site_logo');
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }
}
