<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use frontend\models\ProductsSearch;
use backend\models\product\Brands;
use backend\models\product\Ribbons;
use backend\models\product\Products;
use backend\models\product\Discounts;
use backend\models\product\Categories;
use backend\models\product\FilterItems;

class ProductController extends Controller
{
    /**
     * Product View Page.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findModelProduct($slug);
        $products = Products::find()
            ->where([
                'group_id' => $model->group_id,
                'status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]
            ])
            ->andWhere(['!=', 'id', $model->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit(10)->all();

        return $this->render('view', [
            'model' => $model,
            'products' => $products,
        ]);
    }

    /**
     * Product Category Page.
     * @param string $slug
     * @return mixed
     */
    public function actionCategory($slug)
    {
        $model = $this->findModelCategory($slug);

        $searchModel = new ProductsSearch();
        $searchModel->category_id = $model->id;

        $queryParams = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->search($queryParams);
        $models = $searchModel->querySearch()->all();
        $productIds = array_unique(ArrayHelper::getColumn($models, 'id'));

        $brands = ArrayHelper::map(Brands::find()
            ->where([
                'status' => Brands::STATUS_ACTIVE,
                'id' => array_unique(ArrayHelper::getColumn($models, 'brand_id')),
            ])
            ->orderBy(['weight' => SORT_ASC])
            ->limit(10)
            ->all(), 'id', 'name');

        $discounts = ArrayHelper::map(Discounts::find()
            ->where([
                'status' => Discounts::STATUS_ACTIVE,
                'id' => array_unique(ArrayHelper::getColumn($models, 'discount_id'))
            ])
            ->orderBy(['percentage' => SORT_ASC])
            ->all(), 'id', 'name');

        $priceRange = [
            'min' => $searchModel->getPriceMin(),
            'max' => $searchModel->getPriceMax()
        ];

        $ribbons = ArrayHelper::map(Ribbons::find()
            ->leftJoin('product_to_ribbon', 'product_to_ribbon.ribbon_id=id')
            ->where([
                'status' => Ribbons::STATUS_ACTIVE,
                'product_to_ribbon.product_id' => $productIds
            ])->all(), 'id', 'name');

        $filters = [];

        $filterItems = FilterItems::find()
            ->leftJoin('product_to_filter', 'product_to_filter.filter_item_id=id')
            ->where([
                'product_to_filter.product_id' => $productIds
            ])
            ->orderBy('id')->all();

        if (!empty($filterItems)) {
            foreach ($filterItems as $item) {
                /** @var FilterItems $item */
                $filters[$item->filter->name] = isset($filters[$item->filter->name]) ?
                    ArrayHelper::merge($filters[$item->filter->name], [$item->id => $item->name]) :
                    [$item->id => $item->name];
            }
        }

        return $this->render('category', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'brands' => $brands,
            'discounts' => $discounts,
            'pagination' => $dataProvider->pagination,
            'priceRange' => $priceRange,
            'ribbons' => $ribbons,
            'filters' => $filters,
            'showFilters' => (count($models) > 0) ?: false,
            'quantityText' => $this->renderQuantityText($dataProvider),
            'viewMode' => Yii::$app->session->get('view_mode'),
        ]);
    }

    /**
     * Find Products model.
     * @param string $slug
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelProduct($slug)
    {
        if (($model = Products::findOne(['slug' => $slug, 'status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]])) !== null) {
            $model->views += 1;
            $model->save();

            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Find Categories model.
     * @param string $slug
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelCategory($slug)
    {
        if (($model = Categories::findOne(['slug' => $slug, 'status' => Categories::STATUS_ACTIVE])) !== null)
            return $model;
        else
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * Render Quantity Text.
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    protected function renderQuantityText($dataProvider)
    {
        if ((($count = $dataProvider->getCount()) > 1) && ($pagination = $dataProvider->getPagination()) !== false) {
            $totalCount = $dataProvider->getTotalCount();
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            if ($begin > $end) $begin = $end;
            $page = $pagination->getPage() + 1;
            $pageCount = $pagination->pageCount;

            return Yii::t('frontend', 'Товары <b>{begin, number}—{end, number}</b> из <b>{totalCount, number}</b>', [
                'begin' => $begin,
                'end' => $end,
                'count' => $count,
                'totalCount' => $totalCount,
                'page' => $page,
                'pageCount' => $pageCount,
            ]);
        }

        return '';
    }

    /**
     * SessionViewMode
     */
    public function actionSessionViewMode()
    {
        if ($post = Yii::$app->request->post()) {
            if (isset($post['viewMode']))
                Yii::$app->session->set('view_mode', $post['viewMode']);
        }

        return Json::encode(true);
    }

    /**
     * Displays Search Page.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new ProductsSearch();
        $text = '';
        $dataProvider = false;
        if ($get = Yii::$app->request->get()) {
            if (strlen($text = strip_tags(trim($get['text']))) >= 2) {
                $searchModel->name = $text;

                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->sort->defaultOrder = ['name' => SORT_ASC];
            }
        }

        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'text' => $text,
        ]);
    }

    /**
     * Ajax Product List.
     * @param string $q
     * @return string
     */
    public function actionSearchProductList($q = null)
    {
        $out = [];

        $models = Products::find()
            ->where(['status' => [Products::STATUS_AVAILABLE, Products::STATUS_NOT_AVAILABLE]])
            ->andWhere(['like', 'name', $q])
            ->orderBy('name')
            ->limit(1000)
            ->all();

        /** @var $model Products */
        foreach ($models as $model) {
            $out[] = [
                'key' => $model->slug,
                'value' => $model->name
            ];
        }

        return Json::encode($out);
    }
}
