<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Settings;
use backend\models\sliders\Sliders;
use backend\models\parts\HtmlParts;
use backend\models\product\Categories;
use backend\models\post\PostCategories;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'title' => Settings::getSettingValue('site_name'),
            'logo' => Yii::$app->request->hostInfo . Settings::getSettingValue('site_logo'),
            'metaKeywords' => Settings::getSettingValue('home_meta_keywords'),
            'metaDescription' => Settings::getSettingValue('home_meta_description'),
            'footerText' => Settings::getSettingValue('home_footer_text'),
            'slider' => Sliders::findOne(['key' => 'home_slider', 'status' => Sliders::STATUS_ACTIVE]),
            'about' => HtmlParts::findOne(['key' => 'home_about', 'status' => HtmlParts::STATUS_ACTIVE]),
            'service' => HtmlParts::findOne(['key' => 'home_service', 'status' => HtmlParts::STATUS_ACTIVE]),
            'recommend' => PostCategories::findOne(['id' => 1, 'status' => PostCategories::STATUS_ACTIVE]),
            'categories' => Categories::findAll(['status' => Categories::STATUS_ACTIVE]),
        ]);
    }
}
