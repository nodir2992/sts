<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use backend\models\product\Products;

/**
 * This is the model class for table "basket".
 *
 * @property integer $user_id
 * @property string $session_id
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Products $product
 */
class Basket extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'basket';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'session_id', 'product_id', 'quantity'], 'required'],
            [['user_id', 'product_id', 'quantity', 'created_at', 'updated_at'], 'integer'],
            [['session_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('model', 'User'),
            'session_id' => Yii::t('model', 'Session'),
            'product_id' => Yii::t('model', 'Product'),
            'quantity' => Yii::t('model', 'Quantity'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    public static function getUserBasketItems()
    {
        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();

        return self::findAll(Yii::$app->user->isGuest ?
            ['session_id' => $session->id] :
            ['user_id' => Yii::$app->user->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id'])
            ->andOnCondition(['status' => Products::STATUS_AVAILABLE]);
    }

    /**
     * ItemPrice
     * @return integer
     */
    public function getItemPrice()
    {
        return $this->product->getPriceBySum();
    }

    /**
     * ItemPriceAsCurrency
     * @return integer
     */
    public function getItemPriceAsCurrency()
    {
        return Yii::$app->formatter->asDecimal($this->getItemPrice(), 0)
            . Yii::t('model', '&nbsp;<i>сум</i>');
    }

    /**
     * ItemTotalPrice
     * @return integer
     */
    public function getItemTotalPrice()
    {
        return $this->product->getPriceBySum() * $this->quantity;
    }

    /**
     * ItemTotalPriceAsCurrency
     * @return integer
     */
    public function getItemTotalPriceAsCurrency()
    {
        return Yii::$app->formatter->asDecimal($this->getItemTotalPrice(), 0)
            . Yii::t('model', '&nbsp;<i>сум</i>');
    }
}
