<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\product\Products;

/**
 * ProductsSearch represents the model behind the search form about `backend\models\product\Products`.
 */
class ProductsSearch extends Products
{
    public $price_range;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'category_id'], 'integer'],
            ['brand_id', 'each', 'rule' => ['integer']],
            ['discount_id', 'each', 'rule' => ['integer']],
            ['ribbon_ids', 'each', 'rule' => ['safe']],
            ['filter_ids', 'each', 'rule' => ['integer']],
            ['price_range', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
        ]);

        $dataProvider->pagination->pageSize = 12;

        if (isset($params['brand']))
            $params['brand_id'] = $params['brand'];

        if (isset($params['discount']))
            $params['discount_id'] = $params['discount'];

        if (isset($params['ribbon']))
            $params['ribbon_ids'] = $params['ribbon'];

        if (isset($params['filter']))
            $params['filter_ids'] = $params['filter'];

        $this->load(['ProductsSearch' => $params]);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        if ($this->category_id) {
            $query->leftJoin('product_to_category', 'product_to_category.product_id=products.id')
                ->andFilterWhere(['product_to_category.category_id' => $this->category_id])
                ->groupBy('product_to_category.product_id');
        }

        if (!empty($this->price_range)) {
            $arrayPrice = explode(',', $this->price_range);
            if (isset($arrayPrice[0]) && isset($arrayPrice[1])) {
                $query->andFilterWhere(['between', 'price', ($arrayPrice[0] / $this->getDollarRate()), ($arrayPrice[1] / $this->getDollarRate())]);
            }
        }

        if ($this->ribbon_ids) {
            $query->leftJoin('product_to_ribbon', 'product_to_ribbon.product_id=products.id')
                ->andFilterWhere(['product_to_ribbon.ribbon_id' => $this->ribbon_ids])
                ->groupBy('product_to_ribbon.product_id');
        }

        if ($this->filter_ids) {
            $query->leftJoin('product_to_filter', 'product_to_filter.product_id=products.id')
                ->andFilterWhere(['product_to_filter.filter_item_id' => $this->filter_ids])
                ->groupBy('product_to_filter.product_id');
        }

        $query->andFilterWhere([
            'brand_id' => $this->brand_id,
            'discount_id' => $this->discount_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->andFilterWhere(['status' => [self::STATUS_AVAILABLE, self::STATUS_NOT_AVAILABLE]]);

        return $dataProvider;
    }

    /**
     * Query Search
     * @param bool $byFilter
     * @return \yii\db\ActiveQuery
     */
    public function querySearch($byFilter = false)
    {
        $query = Products::find();

        if ($this->category_id) {
            $query->leftJoin('product_to_category', 'product_to_category.product_id=products.id')
                ->andFilterWhere(['product_to_category.category_id' => $this->category_id])
                ->groupBy('product_to_category.product_id');
        }

        if ($byFilter) {
            $query->andFilterWhere([
                'brand_id' => $this->brand_id,
                'discount_id' => $this->discount_id,
            ]);

            if ($this->ribbon_ids) {
                $query->leftJoin('product_to_ribbon', 'product_to_ribbon.product_id=products.id')
                    ->andFilterWhere(['product_to_ribbon.ribbon_id' => $this->ribbon_ids])
                    ->groupBy('product_to_ribbon.product_id');
            }

            if ($this->filter_ids) {
                $query->leftJoin('product_to_filter', 'product_to_filter.product_id=products.id')
                    ->andFilterWhere(['product_to_filter.filter_item_id' => $this->filter_ids])
                    ->groupBy('product_to_filter.product_id');
            }
        }

        $query->andFilterWhere(['status' => [self::STATUS_AVAILABLE, self::STATUS_NOT_AVAILABLE]]);

        return $query;
    }

    /**
     * Get Price Min
     * @return mixed
     */
    public function getPriceMin()
    {
        return round($this->querySearch(true)->min('price') * $this->getDollarRate());
    }

    /**
     * Get Price Max
     * @return mixed
     */
    public function getPriceMax()
    {
        return round($this->querySearch(true)->max('price') * $this->getDollarRate());
    }
}
