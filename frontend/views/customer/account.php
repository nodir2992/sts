<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \frontend\models\Account */

$this->title = Yii::t('frontend', 'Учетная запись');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="content">
        <h3 class="title"><?= $this->title ?></h3>
        <p>
            <?= Html::a(Yii::t('frontend', 'История заказов'), ['customer/orders'], ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('frontend', 'Изменить учетную запись'), ['customer/account-update'], ['class' => 'btn btn-default']) ?>
        </p>
        <div class="row">
            <div class="col-lg-6 col-md-8">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'firstName',
                        'lastName',
                        'email',
                        'phone',
                        'district',
                        'address',
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
