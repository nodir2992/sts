<?php

use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\AccountUpdateForm */

$this->title = Yii::t('frontend', 'Изменить учетную запись');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="customer-account-update">
    <div class="container">
        <div class="content">

            <h3 class="title"><?= Html::encode($this->title) ?></h3>

            <?php $form = ActiveForm::begin(['id' => 'form-account-update']); ?>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'first_name') ?>
                    <?= $form->field($model, 'last_name') ?>
                    <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
                        ['mask' => '+\\9\\9899-999-9999']) ?>
                    <?= $form->field($model, 'location_id')->dropDownList($model->getLocationsList(),
                        ['prompt' => Yii::t('frontend', 'Выбрать ...')]) ?>
                    <?= $form->field($model, 'address') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'password_new')->passwordInput() ?>
                    <?= $form->field($model, 'password_repeat')->passwordInput() ?>

                    <?= Html::submitButton(Yii::t('frontend', 'Обновить'),
                        ['class' => 'btn btn-success pull-right', 'name' => 'update-button']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
