<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\models\Basket;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $models \frontend\models\Basket */
/* @var $basketIcon string */

$this->title = Yii::t('frontend', 'Корзина');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-basket">
    <div class="container">

        <?php
        if (!empty($models)) {
            Pjax::begin(['id' => 'page_basket']);

            $totalPrice = 0;
            $strItems = '';
            /** @var Basket $item */
            foreach ($models as $item) {
                if ($item->product !== null) {
                    $minQty = 1;
                    $maxQty = $item->product->quantity ?: 100;
                    $strItems .= '<tr><td>'
                        . Html::a(Html::img($item->product->getDefaultImageUrl(), ['alt' => 'product-image'])
                            . '<span>' . $item->product->name . '</span>', ['product/view', 'slug' => $item->product->slug], ['class' => 'item', 'data-pjax' => 0])
                        . '</td><td><span>'
                        . $item->getItemPriceAsCurrency()
                        . '</span></td><td>'
                        . TouchSpin::widget([
                            'name' => 'product[' . $item->product_id . ']',
                            'value' => $item->quantity,
                            'readonly' => true,
                            'options' => ['class' => 'input-sm'],
                            'pluginOptions' => [
                                'min' => $minQty,
                                'max' => $maxQty,
                                'buttonup_txt' => '<i class="glyphicon glyphicon-plus"></i>',
                                'buttondown_txt' => '<i class="glyphicon glyphicon-minus"></i>',
                                'buttonup_class' => ($item->quantity == $maxQty) ? 'disabled btn btn-default' : 'btn btn-default',
                                'buttondown_class' => ($item->quantity == $minQty) ? 'disabled btn btn-default' : 'btn btn-default',
                            ],
                            'pluginEvents' => [
                                'touchspin.on.startspin' => 'function(e) {                                    
                                    if (e.currentTarget.defaultValue != e.currentTarget.value){
                                        var overlay = $(".overlay");
                                        overlay.show();
                                        $("#form_basket").submit();
                                        $.pjax.reload({
                                            container: "#header_basket"
                                        }).done(function () {
                                            $.pjax.reload({
                                                container: "#page_basket"
                                            }).done(function () {
                                                overlay.hide();
                                            });
                                        });
                                    }
                                }',
                            ]
                        ])
                        . '</td><td><span>'
                        . $item->getItemTotalPriceAsCurrency()
                        . '</span></td><td>'
                        . Html::a('<i class="fa fa-remove"></i>', ['customer/remove-basket-item', 'id' => $item->product_id], ['class' => 'item-remove', 'data-pjax' => 0])
                        . '</td></tr>';
                }
                $totalPrice += $item->getItemTotalPrice();
            } ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="head">
                        <div class="row">
                            <div class="col-sm-5">
                                <h1 class="title">
                                    <?= Html::img($basketIcon, ['alt' => 'basket']) . $this->title ?>
                                </h1>
                            </div>
                            <div class="col-sm-7 right">
                                <span><strong><?= Yii::t('frontend', 'Общая сумма:') ?> </strong>
                                    <?= Yii::$app->formatter->asDecimal($totalPrice, 0) . Yii::t('model', '&nbsp;<i>сум</i>') ?></span>
                                <?= Html::a(Yii::t('frontend', 'Оформить заказ'), ['customer/checkout'], ['class' => 'btn btn-success', 'data-pjax' => 0]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="body">
                        <?= Html::beginForm('', '', ['data-pjax' => true, 'id' => 'form_basket']) ?>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th><?= Yii::t('frontend', 'Название товара') ?></th>
                                    <th width="150"><?= Yii::t('frontend', 'Цена') ?></th>
                                    <th width="150"><?= Yii::t('frontend', 'Количество') ?></th>
                                    <th width="150"><?= Yii::t('frontend', 'Итоговая цена') ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?= $strItems ?>
                                </tbody>
                            </table>
                        </div>

                        <?= Html::endForm() ?>
                    </div>
                </div>
            </div>

            <?php
            Pjax::end();
        } else { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="head">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="title">
                                    <?= Html::img($basketIcon, ['alt' => 'basket']) . $this->title ?>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="body">
                        <p><?= Yii::t('frontend', 'Ваша корзина для покупок пуста.') ?></p>
                        <?= Html::a(Yii::t('frontend', 'Вернуться на главную'), ['/']) ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
