<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


$this->title = Yii::t('frontend', 'Вход');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="customer-login">
    <div class="container">
        <div class="content">
            <h3 class="title"><?= Html::encode($this->title) ?></h3>

            <p><?= Yii::t('frontend', 'Пожалуйста, заполните следующие поля для входа:') ?></p>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>

                    <p><?= Html::a(Yii::t('frontend', 'Забыли пароль?'), ['request-password-reset']) ?></p><br>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('frontend', 'Войти'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
