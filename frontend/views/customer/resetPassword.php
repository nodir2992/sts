<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = Yii::t('frontend', 'Сбросить пароль');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="customer-reset-password">
    <div class="container">
        <div class="content">
            <h3 class="title"><?= Html::encode($this->title) ?></h3>

            <p><?= Yii::t('frontend', 'Выберите новый пароль:') ?></p>

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'password_repeat')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('frontend', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
