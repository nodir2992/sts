<?php

/* @var $this yii\web\View */
/* @var $content string */
?>

<?php if (isset($this->params['breadcrumbs'])) { ?>
    <!-- breadcrumbs -->
    <div class="container">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => $this->params['breadcrumbs'],
        ]) ?>
    </div>
    <!-- //breadcrumbs -->
<?php } ?>

<?= $content ?>

<?= \frontend\widgets\AlertGrowl::widget() ?>
