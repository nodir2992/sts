<?php

use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\menu\Menus;
use backend\models\parts\HtmlParts;

/* @var $this yii\web\View */
?>

<?php if (isset($this->params['footer_text'])) { ?>
    <div class="footer-text">
        <div class="container">
            <?= $this->params['footer_text'] ?>
            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>

<footer class="footer">
    <div class="ft-top">
        <div class="container">
            <div class="row">
                <div class="col-md-8 menu">
                    <div class="row">

                        <?php if (($ftInfoMenu = Menus::findOne(['key' => 'footer_info', 'status' => Menus::STATUS_ACTIVE])) !== null) { ?>
                            <div class="col-sm-4">
                                <ul class="list-unstyled">
                                    <li><strong><?= $ftInfoMenu->name ?></strong></li>
                                    <?php if (!empty($ftInfoMenuItems = $ftInfoMenu->activeMenuItems)) {
                                        foreach ($ftInfoMenuItems as $item) {
                                            echo '<li>' . Html::a($item->label, Url::to($item->url)) . '</li>';
                                        }
                                    } ?>
                                </ul>
                            </div>
                        <?php }

                        if (($ftServiceMenu = Menus::findOne(['key' => 'footer_service', 'status' => Menus::STATUS_ACTIVE])) !== null) { ?>
                            <div class="col-sm-4">
                                <ul class="list-unstyled">
                                    <li><strong><?= $ftServiceMenu->name ?></strong></li>
                                    <?php if (!empty($ftServiceMenuItems = $ftServiceMenu->activeMenuItems)) {
                                        foreach ($ftServiceMenuItems as $item) {
                                            echo '<li>' . Html::a($item->label, Url::to($item->url)) . '</li>';
                                        }
                                    } ?>
                                </ul>
                            </div>
                        <?php }

                        if ((($customerMenu = Menus::findOne(['key' => 'customer', 'status' => Menus::STATUS_ACTIVE])) !== null) && !Yii::$app->user->isGuest) { ?>
                            <div class="col-sm-4">
                                <ul class="list-unstyled">
                                    <li><strong><?= $customerMenu->name ?></strong></li>
                                    <?php if (!empty($customerMenuItems = $customerMenu->activeMenuItems)) {
                                        foreach ($customerMenuItems as $item) {
                                            echo '<li>' . Html::a($item->label, Url::to($item->url)) . '</li>';
                                        }
                                    } ?>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <div class="col-sm-4">
                                <ul class="list-unstyled">
                                    <li><strong><?= Yii::t('frontend', 'Личный кабинет') ?></strong></li>
                                    <li><?= Html::a(Yii::t('frontend', 'Войти'), ['customer/login']) ?></li>
                                    <li><?= Html::a(Yii::t('frontend', 'Регистрация'), ['customer/signup']) ?></li>
                                </ul>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?= (($footerContact = HtmlParts::findOne(['key' => 'footer_contact', 'status' => HtmlParts::STATUS_ACTIVE])) !== null) ? $footerContact->body : '' ?>
            </div>
        </div>
    </div>

    <?= (($footerBottom = HtmlParts::findOne(['key' => 'footer_bottom', 'status' => HtmlParts::STATUS_ACTIVE])) !== null) ? $footerBottom->body : '' ?>
</footer>
