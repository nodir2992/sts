<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\models\Settings;
use backend\models\menu\Menus;
use backend\models\parts\HtmlParts;

/* @var $this yii\web\View */
?>

<header class="header">
    <div class="hd-bar">
        <div class="container">
            <?= (($headerContact = HtmlParts::findOne(['key' => 'header_contact', 'status' => HtmlParts::STATUS_ACTIVE])) !== null) ? $headerContact->body : '' ?>

            <?php if ((($customerMenu = Menus::findOne(['key' => 'customer', 'status' => Menus::STATUS_ACTIVE])) !== null) && !Yii::$app->user->isGuest) { ?>
                <div class="dropdown pull-right customer">
                    <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">
                        <i class="fa fa-user"></i><span><?= $customerMenu->name ?> <i
                                    class="fa fa-angle-down"></i></span></button>
                    <ul class="dropdown-menu">
                        <?php if (!empty($customerMenuItems = $customerMenu->activeMenuItems)) {
                            foreach ($customerMenuItems as $item) {
                                echo '<li>' . Html::a($item->label, Url::to($item->url)) . '</li>';
                            }
                            echo '<li>'
                                . Html::beginForm(['/customer/logout'], 'post')
                                . Html::submitButton(Yii::t('frontend', 'Выйти'), ['class' => 'btn btn-link logout'])
                                . Html::endForm()
                                . '</li>';
                        } ?>
                    </ul>
                </div>
            <?php } else { ?>
                <ul class="list-inline guest-customer pull-right">
                    <li><?= Html::a(Yii::t('frontend', 'Войти'), ['customer/login']) ?></li>
                    <li>|</li>
                    <li><?= Html::a(Yii::t('frontend', 'Регистрация'), ['customer/signup']) ?></li>
                </ul>
            <?php } ?>
        </div>
    </div>
    <div class="hd-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-md-3 logo">
                    <?php if ($logo = Settings::getSettingValue('site_logo')) {
                        echo Html::a(Html::img($logo, ['alt' => 'logo']), ['/']);
                    } ?>
                </div>
                <div class="col-sm-7 col-md-6 search">
                    <?= Html::beginForm(['product/search'], 'get', ['class' => 'search-form'])
                    . '<div class="input-group">'
                    . \kartik\typeahead\Typeahead::widget([
                        'name' => 'text',
                        'options' => [
                            'placeholder' => Yii::t('frontend', 'Поиск'),
                            'required' => true,
                            'type' => 'search',
                        ],
                        'scrollable' => true,
                        'pluginOptions' => [
                            'highlight' => true,
                            'minLength' => 2,
                        ],
                        'dataset' => [
                            [
                                'display' => 'value',
                                'limit' => 1000,
                                'remote' => [
                                    'url' => Url::to(['product/search-product-list']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ],
                        'pluginEvents' => [
                            'typeahead:select' => 'function(event, data) {
                                    location.href = "/product/view/" + data["key"];
                                }',
                        ]
                    ])
                    . '<div class="input-group-btn">'
                    . Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-default form-btn'])
                    . '</div></div>'
                    . Html::endForm() ?>
                </div>
                <div class="col-sm-3 col-md-3 basket">
                    <?php
                    $basketIcon = Settings::getSettingValue('basket_icon');
                    Pjax::begin(['id' => 'header_basket']);
                    $qty = 0;
                    $totalPrice = 0;
                    $strItems = '';
                    if (!empty($basketItems = \frontend\models\Basket::getUserBasketItems())) {
                        /** @var $item \frontend\models\Basket */
                        foreach ($basketItems as $item) {
                            if ($item->product !== null) {
                                $strItems .= '<li>'
                                    . Html::a('<strong>' . $item->quantity . 'x</strong>'
                                        . Html::img($item->product->getDefaultImageUrl(), ['alt' => 'product']) . '<span>' . $item->product->name . '</span>',
                                        ['/product/view', 'slug' => $item->product->slug], ['class' => 'item', 'data-pjax' => 0])
                                    . Html::a('<i class="fa fa-remove"></i>',
                                        ['customer/remove-basket-item', 'id' => $item->product_id], ['class' => 'item-remove', 'data-pjax' => 0])
                                    . '</li>';

                                $qty += $item->quantity;
                                $totalPrice += $item->getItemTotalPrice();
                            }
                        }

                        $strItems = '<ul class="list-unstyled">'
                            . $strItems
                            . '<li><p class="total-price"><strong>'
                            . Yii::t('frontend', 'Всего к оплате: ')
                            . Yii::$app->formatter->asDecimal($totalPrice, 0)
                            . Yii::t('model', '&nbsp;<i>сум</i>')
                            . '</strong><br>'
                            . Html::a(Yii::t('frontend', 'Корзина'), ['customer/basket'], ['data-pjax' => 0])
                            . '</p></li></ul>';
                    } else {
                        $strItems = '<ul class="list-unstyled"><li><p><br>'
                            . Yii::t('frontend', 'Ваша корзина пуста.')
                            . '</p></li></ul>';
                    }
                    echo '<div class="basket-items pull-right">' . Html::a(Html::img($basketIcon, ['alt' => 'basket', 'class' => 'pull-left'])
                            . '<span>' . Yii::t('frontend', 'Корзина')
                            . '<br><span>' . Yii::t('frontend', 'Товаров ')
                            . '(' . $qty . ')</span></span>', ['customer/basket'], ['data-pjax' => 0, 'class' => 'icon'])
                        . $strItems
                        . '</div>';

                    Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="hd-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-3 categories-menu">
                    <?php if (($categoriesMenu = Menus::findOne(['key' => 'categories', 'status' => Menus::STATUS_ACTIVE])) !== null) { ?>
                        <button type="button" data-toggle="#categories_menu">
                            <?= $categoriesMenu->name ?><i class="fa fa-bars pull-right"></i></button>
                        <?php if (!empty($categoriesMenuItems = $categoriesMenu->activeMenuItems)) { ?>
                            <ul id="categories_menu" class="list-unstyled">
                                <?php /** @var $item \backend\models\menu\MenuItems */
                                foreach ($categoriesMenuItems as $item) {
                                    echo '<li>' . Html::a($item->label, Url::to($item->url)) . '</li>';
                                } ?>
                            </ul>
                        <?php }
                    } ?>
                </div>
                <div class="col-md-9 main-menu">
                    <?php if (($mainMenu = Menus::findOne(['key' => 'main_menu', 'status' => Menus::STATUS_ACTIVE])) !== null) { ?>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#main_menu" aria-expanded="false">
                                <span class="text"><?= $mainMenu->name ?></span>
                                <i class="fa fa-bars pull-right"></i></button>
                        </div>
                        <?php if (!empty($mainMenuItems = $mainMenu->activeMenuItems)) { ?>
                            <div id="main_menu" class="navbar-collapse collapse" aria-expanded="false">
                                <ul class="navbar-nav nav">
                                    <?php foreach ($mainMenuItems as $item) {
                                        echo '<li>' . Html::a(Html::img($item->icon, ['alt' => 'icon']) . $item->label, Url::to($item->url)) . '</li>';
                                    } ?>
                                </ul>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </div>
</header>
