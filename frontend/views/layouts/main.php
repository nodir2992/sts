<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\StsAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php
    if (isset($this->params['meta_keywords']))
        $this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords']]);
    if (isset($this->params['meta_description']))
        $this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description']]);
    $this->registerMetaTag(['property' => 'og:site_name', 'content' => Yii::$app->name]);
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:locale', 'content' => Yii::$app->language]);
    if (isset($this->params['meta_type']))
        $this->registerMetaTag(['property' => 'og:type', 'content' => $this->params['meta_type']]);
    if (isset($this->params['meta_url']))
        $this->registerMetaTag(['property' => 'og:url', 'content' => $this->params['meta_url']]);
    if (isset($this->params['meta_image']))
        $this->registerMetaTag(['property' => 'og:image', 'content' => $this->params['meta_image']]);
    if (isset($this->params['meta_description']))
        $this->registerMetaTag(['property' => 'og:description', 'content' => $this->params['meta_description']]); ?>
    <?php $this->head() ?>
</head>
<body<?= isset($this->params['background_line']) ? ' class="line"' : '' ?>>
<?php $this->beginBody() ?>

<!-- header -->
<?= $this->render('header') ?>
<!-- //header -->

<?= $this->render('content', [
    'content' => $content
]) ?>

<!-- footer -->
<?= $this->render('footer') ?>
<!-- //footer -->

<div class="overlay">
    <div class="spinner"></div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
