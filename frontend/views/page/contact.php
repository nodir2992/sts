<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $modelForm \frontend\models\ContactForm */
/* @var $model \backend\models\page\Pages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;

$this->params['meta_type'] = 'page';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/page/' . $model->url;
$this->params['meta_image'] = $model->siteLogo;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
?>

<div class="page-contact page-view">
    <div class="container">
        <div class="content">
            <h3 class="title"><?= Html::encode($model->name) ?></h3>

            <div class="row">
                <div class="col-md-6">
                    <?= $model->body ?>
                </div>
                <div class="col-md-5">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($modelForm, 'name') ?>

                    <?= $form->field($modelForm, 'email') ?>

                    <?= $form->field($modelForm, 'subject') ?>

                    <?= $form->field($modelForm, 'body')->textarea(['rows' => 6]) ?>

                    <?= $form->field($modelForm, 'verifyCode')->widget(Captcha::class, [
                        'template' => '<div class="row"><div class="col-md-6">{input}</div></div>{image}<button type="button" id="captcha_refresh" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i></button>',
                        'imageOptions' => ['id' => 'captcha_image']
                    ]) ?>
                    <?php $this->registerJs("jQuery('#captcha_refresh').on('click', function(e){
                    e.preventDefault(); jQuery('#captcha_image').yiiCaptcha('refresh'); })") ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('frontend', 'Отправить'), ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
