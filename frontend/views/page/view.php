<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\models\page\Pages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $model->name;

$this->params['meta_type'] = 'page';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/page/' . $model->url;
$this->params['meta_image'] = $model->siteLogo;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
?>

<div class="page-view">
    <div class="container">
        <div class="content">
            <h3 class="title"><?= Html::encode($model->name) ?></h3>
            <?= $model->body ?>
        </div>
    </div>
</div>
