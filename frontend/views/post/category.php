<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use rmrevin\yii\fontawesome\FA;
use backend\models\post\PostCategories;

/* @var $this yii\web\View */
/* @var $model PostCategories */

$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = strip_tags($model->name);

$this->params['meta_type'] = 'post-category';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/post/category/' . $model->slug;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
if (!empty($model->image))
    $this->params['meta_image'] = Yii::$app->request->hostInfo . $model->image;
if ($model->description)
    $this->params['footer_text'] = $model->description;
?>

<div class="post-category">
    <div class="container">
        <div class="head">
            <h3 class="title"><?= strip_tags($model->name) ?></h3>
        </div>
        <div class="content">
            <?php Pjax::begin(); ?>
            <?= ListView::widget([
                'dataProvider' => $model->getPostsDataProvider(),
                'layout' => "{items}\n<div align='center'>{pager}</div>",
                'itemView' => '_item',
                'itemOptions' => ['class' => 'row item'],
                'pager' => [
                    'class' => LinkPager::class,
                    'firstPageLabel' => FA::icon(FA::_ANGLE_DOUBLE_LEFT),
                    'prevPageLabel' => FA::icon(FA::_ANGLE_LEFT),
                    'nextPageLabel' => FA::icon(FA::_ANGLE_RIGHT),
                    'lastPageLabel' => FA::icon(FA::_ANGLE_DOUBLE_RIGHT),
                    'maxButtonCount' => 7,
                ]
            ]) ?>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
