<?php

use yii\helpers\Html;
use backend\models\post\Posts;

/* @var $this yii\web\View */
/* @var $model Posts */

$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = ['label' => strip_tags($model->category->name), 'url' => ['category', 'slug' => $model->category->slug]];
$this->params['breadcrumbs'][] = strip_tags($model->title);

$this->params['meta_type'] = 'post';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/post/view/' . $model->slug;
$this->params['meta_image'] = Yii::$app->request->hostInfo . $model->image;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
?>

<div class="post-view">
    <div class="container">
        <div class="head">
            <h3 class="title"><?= strip_tags($model->title) ?></h3>
        </div>
        <div class="content">
            <?= $model->body ?>
        </div>

        <?php if (!empty($model->products)) { ?>
            <div class="content products">
                <h4><span><?= Yii::t('frontend', 'Товары из этого обзора') ?></span></h4>

                <?php foreach ($model->products as $product) { ?>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-2 image">
                                <?= Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product']), ['product/view', 'slug' => $product->slug], ['class' => 'thumbnail']) ?>
                            </div>
                            <div class="col-md-7 info">
                                <?= Html::a($product->name, ['product/view', 'slug' => $product->slug]) ?>
                                <p class="summary"><?= strip_tags($product->detail) ?></p>
                            </div>
                            <div class="col-md-3 info">
                                <?= $product->getOldPriceFrontend()
                                . $product->getPriceFrontend() . '<br>'
                                . $product->addToCartBtn() ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

    </div>
</div>
