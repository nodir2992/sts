<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use kartik\slider\Slider;
use rmrevin\yii\fontawesome\FA;

/** @var $this yii\web\View */
/** @var $model \backend\models\product\Categories */
/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $quantityText string */
/** @var $pagination \yii\data\Pagination */
/** @var $viewMode string|null */
/** @var $showFilters bool */

Pjax::begin(['id' => 'category_products', 'timeout' => 3000]);
$queryParams = Yii::$app->request->queryParams;

$title = $model->name;
$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = $title;

$this->params['meta_type'] = 'product-category';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/product/category/' . $model->slug;
if ($model->image)
    $this->params['meta_image'] = Yii::$app->request->hostInfo . $model->image;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
?>

<!-- product category -->
<div class="product-category">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="head">
                    <div class="row">
                        <div class="col-lg-8 col-md-7">
                            <h1 class="title"><?= $title ?></h1>
                        </div>
                        <?php if ($showFilters) { ?>
                            <div class="col-lg-4 col-md-5">
                                <div class="dropdown products-sort pull-left">
                                    <button class="btn btn-link dropdown-toggle" type="button"
                                            data-toggle="dropdown"><?= Yii::t('frontend', 'Сортировать по') ?>
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><?= $dataProvider->sort->link('price', ['label' => Yii::t('frontend', 'Цена'), 'class' => 'sort-ordinal']) ?></li>
                                        <li><?= $dataProvider->sort->link('name', ['label' => Yii::t('frontend', 'Название')]) ?></li>
                                    </ul>
                                </div>
                                <div class="view-mode pull-right">
                                    <?php if ($viewMode == 'list') { ?>
                                        <button class="btn btn-link" data-view="grid">
                                            <i class="fa fa-th"></i> <?= Yii::t('frontend', 'Сетка') ?>
                                        </button>
                                        <span></span>
                                        <button class="btn btn-link active" data-view="list">
                                            <i class="fa fa-th-list"></i> <?= Yii::t('frontend', 'Список') ?>
                                        </button>
                                    <?php } else { ?>
                                        <button class="btn btn-link active" data-view="grid">
                                            <i class="fa fa-th"></i> <?= Yii::t('frontend', 'Сетка') ?>
                                        </button>
                                        <span></span>
                                        <button class="btn btn-link" data-view="list">
                                            <i class="fa fa-th-list"></i> <?= Yii::t('frontend', 'Список') ?>
                                        </button>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php if ($showFilters) { ?>
                <div class="col-md-3 col-sm-4 filter">
                    <div>
                        <h4><?= Yii::t('frontend', 'Фильтры') ?></h4>
                        <?php
                        echo Html::beginForm(Url::to(['category', 'slug' => $queryParams['slug']]), 'get', ['data-pjax' => true, 'id' => 'form_filter']);

                        /** @var $priceRange array */
                        if ($priceRange && $priceRange['max'] != $priceRange['min']) {
                            echo '<hr><h5>' . Yii::t('frontend', 'Стоимость') . '</h5><div class="price-range">'
                                . Slider::widget([
                                    'name' => 'price_range',
                                    'value' => (isset($queryParams['price_range']) && !empty($queryParams['price_range'])) ?
                                        $queryParams['price_range'] : $priceRange['min'] . ',' . $priceRange['max'],
                                    'pluginOptions' => [
                                        'min' => $priceRange['min'],
                                        'max' => $priceRange['max'],
                                        'step' => 100
                                    ],
                                    'pluginEvents' => [
                                        'slideStop' => 'function(event) {                                                                 
                                        overlay.show();
                                        formFilter.submit();
                                        $.pjax.reload({
                                            container:"#category_products"
                                        }).done(function() {
                                            overlay.hide();
                                        });
                                    }',
                                    ]
                                ])
                                . '<strong>' . Yii::$app->formatter->asDecimal($priceRange['min'], 0) . ' – '
                                . Yii::$app->formatter->asDecimal($priceRange['max'], 0) . '&nbsp;'
                                . Yii::t('frontend', 'сум') . '</strong></div>';
                        }

                        /** @var $brands array */
                        if (count($brands) > 1) {
                            echo '<hr><h5>' . Yii::t('frontend', 'Бренды') . '</h5>'
                                . Html::checkboxList('brand', isset($queryParams['brand']) ? $queryParams['brand'] : null, $brands, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        $checked = $checked ? 'checked' : '';
                                        return "<div class='checkbox'><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></div>";
                                    },
                                    'class' => 'checkbox-list'
                                ]);
                        }

                        /** @var $discounts array */
                        if (!empty($discounts)) {
                            echo '<hr><h5>' . Yii::t('frontend', 'Скидки') . '</h5>'
                                . Html::checkboxList('discount', isset($queryParams['discount']) ? $queryParams['discount'] : null, $discounts, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        $checked = $checked ? 'checked' : '';
                                        return "<div class='checkbox'><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></div>";
                                    },
                                    'class' => 'checkbox-list'
                                ]);
                        }

                        /** @var $ribbons array */
                        if (!empty($ribbons)) {
                            echo '<hr><h5>' . Yii::t('frontend', 'Наши предложения') . '</h5>'
                                . Html::checkboxList('ribbon', isset($queryParams['ribbon']) ? $queryParams['ribbon'] : null, $ribbons, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        $checked = $checked ? 'checked' : '';
                                        return "<div class='checkbox'><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></div>";
                                    },
                                    'class' => 'checkbox-list'
                                ]);
                        }

                        /** @var $filters array */
                        if (!empty($filters)) {
                            foreach ($filters as $filter => $items) {
                                echo '<hr><h5>' . $filter . '</h5>'
                                    . Html::checkboxList('filter', isset($queryParams['filter']) ? $queryParams['filter'] : null, $items, [
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                            $checked = $checked ? 'checked' : '';
                                            return "<div class='checkbox'><label><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label}</label></div>";
                                        },
                                        'class' => 'checkbox-list'
                                    ]);
                            }
                        }

                        if (isset($queryParams['sort'])) {
                            echo Html::hiddenInput('sort', $queryParams['sort']);
                        }

                        echo Html::endForm();

                        $this->registerJs('
                        var formFilter = $("#form_filter");
                        var overlay = $(".overlay");
                        
                        $(".checkbox").find("input").click(function () {
                            overlay.show();
                            $(".price-range").find("input").val("");
                            formFilter.submit();
                            $.pjax.reload({
                                container:"#category_products"
                            }).done(function() {
                                overlay.hide();
                            });
                        });
                        
                        $(".view-mode").find("button").click(function () {
                            overlay.show();
                            $.ajax({
                                type : "POST",
                                url: "/product/session-view-mode",
                                data: {viewMode: $(this).data("view")},
                                success  : function(response) { 
                                    $.pjax.reload({
                                        container:"#category_products"
                                    }).done(function() {
                                        overlay.hide();
                                    });
                                }
                            });
                        });
                    '); ?>
                        <hr>
                        <p align="right"><?= $quantityText ?></p>
                        <p align="center">
                            <?= Html::a(Yii::t('frontend', 'Очистить все'),
                                Url::to(['category', 'slug' => $queryParams['slug']]), ['class' => 'btn btn-default']) ?>
                        </p>
                        <div class="clearfix"></div>
                    </div>
                </div>
            <?php }
            if (!empty($dataProvider->models)) {
                if ($viewMode == 'list') { ?>
                    <div class="col-md-9 col-sm-8 list-view">
                        <?php /** @var $product \backend\models\product\Products */
                        foreach ($dataProvider->models as $product) { ?>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4 img">
                                        <?= Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product']),
                                            ['view', 'slug' => $product->slug], ['data-pjax' => 0]) ?>
                                    </div>
                                    <div class="col-md-9 col-sm-8 info">
                                        <div>
                                            <?= Html::a($product->name, ['view', 'slug' => $product->slug], ['data-pjax' => 0]) ?>
                                            <p class="summary"><?= strip_tags($product->detail) ?></p>
                                            <?= $product->getOldPriceFrontend()
                                            . $product->getPriceFrontend()
                                            . $product->addToCartBtn() ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                <?php } else { ?>
                    <div class="col-md-9 col-sm-8 grid-view">
                        <?php /** @var $product \backend\models\product\Products */
                        foreach ($dataProvider->models as $product) { ?>
                            <div class="col-md-4 col-sm-6 item">
                                <div>
                                    <?= Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product']) . "<span>$product->name</span>",
                                        ['view', 'slug' => $product->slug], ['data-pjax' => 0]) ?>
                                    <p class="summary"><?= mb_substr(strip_tags($product->detail), 0, 90) ?>...</p>
                                    <?= $product->getOldPriceFrontend()
                                    . $product->getPriceFrontend()
                                    . $product->addToCartBtn() ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                <?php }
            } else {
                echo '<div class="col-sm-9"><strong>' . Yii::t('frontend', 'Результатов не найдено.') . '</strong></div>';
            } ?>

            <div class="col-sm-9 col-sm-offset-3">
                <div align="center">
                    <?= LinkPager::widget([
                        'pagination' => $pagination,
                        'firstPageLabel' => FA::icon(FA::_ANGLE_DOUBLE_LEFT),
                        'prevPageLabel' => FA::icon(FA::_ANGLE_LEFT),
                        'nextPageLabel' => FA::icon(FA::_ANGLE_RIGHT),
                        'lastPageLabel' => FA::icon(FA::_ANGLE_DOUBLE_RIGHT),
                        'maxButtonCount' => 7,
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
</div>

<?php if ($model->description && (!isset($queryParams['page']) || (isset($queryParams['page']) && $queryParams['page'] == 1))) { ?>
    <div class="footer-text">
        <div class="container">
            <?= $model->description ?>
            <div class="clearfix"></div>
        </div>
    </div>
<?php } ?>
<!-- //product category -->
<?php Pjax::end() ?>
