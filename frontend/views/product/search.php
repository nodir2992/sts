<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use kartik\typeahead\Typeahead;

/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $text string */

$this->title = Yii::t('frontend', 'Результат поиска');

$this->params['breadcrumbs'][] = ['label' => 'Все товары', 'url' => ['category', 'slug' => 'all']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-search">
    <div class="container">
        <div class="content">

            <div class="row">
                <div class="col-md-8 col-lg-6">
                    <?= Html::beginForm(['product/search'], 'get', ['class' => 'search-form'])
                    . '<div class="input-group">'
                    . Typeahead::widget([
                        'name' => 'text',
                        'options' => [
                            'placeholder' => Yii::t('frontend', 'Поиск'),
                            'required' => true,
                            'type' => 'search',
                        ],
                        'value' => $text ?: false,
                        'scrollable' => true,
                        'pluginOptions' => [
                            'highlight' => true,
                            'minLength' => 2,
                        ],
                        'dataset' => [
                            [
                                'display' => 'value',
                                'limit' => 1000,
                                'remote' => [
                                    'url' => Url::to(['product/search-product-list']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ],
                        'pluginEvents' => [
                            'typeahead:select' => 'function(event, data) {
                                    location.href = "/product/view/" + data["key"];
                                }',
                        ]
                    ])
                    . '<div class="input-group-btn">'
                    . Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-default form-btn'])
                    . '</div></div>'
                    . Html::endForm() ?>
                </div>
            </div>

            <?php if ($dataProvider) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="w3ls-title"><?= $this->title ?></h4>
                        <p>
                            <?= ($dataProvider->totalCount > 0) ? '<strong>' . Yii::t('frontend', 'Найдено') . ':</strong> '
                                . $dataProvider->totalCount : Yii::t('frontend', 'Результатов не найдено') ?>
                        </p>
                    </div>
                </div>

                <?php if (!empty($dataProvider->models)) { ?>
                    <div class="list-group products">
                        <?php /** @var $product \backend\models\product\Products */
                        foreach ($dataProvider->models as $product) { ?>
                            <div class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-4 col-md-3 image">
                                        <?= Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product']), ['product/view', 'slug' => $product->slug]) ?>
                                    </div>
                                    <div class="col-sm-8 info">
                                        <p>
                                            <?= Html::a(str_ireplace(mb_strtolower($text), '<span>' . $text . '</span>', mb_strtolower($product->name)),
                                                ['product/view', 'slug' => $product->slug]) ?>
                                        </p>
                                        <p class="summary"><?= strip_tags($product->detail) ?></p>
                                        <?= $product->getOldPriceFrontend()
                                        . $product->getPriceFrontend() . '<p>'
                                        . $product->addToCartBtn() . '</p>' ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="clearfix"></div>
                <div align="center">
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]) ?>
                </div>
            <?php } ?>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
