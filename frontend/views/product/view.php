<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\models\product\Products */
/* @var $products array */

\frontend\assets\XzoomAsset::register($this);

$this->title = $model->meta_title;
if (isset($model->categories[0])) {
    $this->params['breadcrumbs'][] = ['label' => $model->categories[0]->name, 'url' => ['category', 'slug' => $model->categories[0]->slug]];
}
$this->params['breadcrumbs'][] = $model->name;

$this->params['meta_type'] = 'product';
$this->params['meta_url'] = Yii::$app->request->hostInfo . '/product/view/' . $model->slug;
if ($model->meta_keywords)
    $this->params['meta_keywords'] = $model->meta_keywords;
if ($model->meta_description)
    $this->params['meta_description'] = $model->meta_description;
if (!empty($model->getDefaultImageUrl()))
    $this->params['meta_image'] = Yii::$app->request->hostInfo . $model->getDefaultImageUrl();
?>
<!-- product detail -->
<div class="product-detail">
    <div class="container">

        <div class="info">
            <div class="col-md-12"><h1 class="title"><?= $model->name ?></h1></div>
            <div class="clearfix"></div>

            <div class="col-md-4 col-sm-5">
                <?php if (!empty($images = $model->productImages)) { ?>
                    <!-- magnific start -->
                    <div class="xzoom-container">
                        <img class="xzoom-img" id="xzoom-magnific"
                             src="<?= $model->getDefaultImageUrl() ?>"
                             xoriginal="<?= $model->getDefaultImageUrl() ?>"/>
                        <div class="xzoom-thumbs">
                            <?php foreach ($images as $image) {
                                echo "<a href='$image->path'><img class='xzoom-gallery' src='$image->path'></a>&nbsp;";
                            } ?>
                        </div>
                    </div>
                    <!-- magnific end -->
                <?php } ?>
            </div>

            <div class="col-md-8 col-sm-7">
                <div class="row">
                    <div class="col-md-8">
                        <?php if ($model->brand->image) echo '<p class="brand">' . Html::img($model->brand->image, ['alt' => 'brand']) . '</p>'; ?>
                        <p><strong><?= Yii::t('frontend', 'Краткое описание') ?></strong></p>
                        <?= $model->detail ?>
                    </div>
                    <div class="col-md-4">
                        <div class="cart">
                            <p><strong><?= Yii::t('frontend', 'Цена') ?></strong></p>
                            <?= $model->getOldPriceFrontend()
                            . $model->getPriceFrontend()
                            . $model->addToCartBtn() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab_1"><i class="fa fa-list-alt"></i>
                            <?= Yii::t('frontend', 'Описание') ?></a></li>
                    <li><a data-toggle="tab" href="#tab_2"><i class="fa fa-list"></i>
                            <?= Yii::t('frontend', 'Характеристики') ?></a></li>
                    <li><a data-toggle="tab" href="#tab_3"><i class="fa fa-credit-card"></i>
                            <?= Yii::t('frontend', 'Оплата и Доставка') ?></a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab_1" class="tab-pane fade in active">
                        <p><?= $model->description ?></p>
                    </div>
                    <div id="tab_2" class="tab-pane fade">
                        <p><?= $model->information ?></p>
                    </div>
                    <div id="tab_3" class="tab-pane fade">
                        <p>Оплата и Доставка</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <?php if (!empty($products)) { ?>
            <!-- similar -->
            <div class="similar">
                <div class="carousel">
                    <h3 class="title"><span><?= Yii::t('frontend', 'Похожие товары') ?></span></h3>
                    <div class="body">
                        <?php /** @var $product \backend\models\product\Products */
                        $carouselItems = [];
                        foreach ($products as $product) {
                            array_push($carouselItems,
                                Html::a(Html::img($product->getDefaultImageUrl(), ['alt' => 'product'])
                                    . "<span>$product->name</span>", ['view', 'slug' => $product->slug])
                                . $product->getOldPriceFrontend()
                                . $product->getPriceFrontend()
                                . $product->addToCartBtn());
                        }

                        echo \evgeniyrru\yii2slick\Slick::widget([
                            'itemContainer' => 'div',
                            'items' => $carouselItems,
                            'itemOptions' => ['class' => 'carousel-item'],
                            'clientOptions' => [
                                'slidesToShow' => 4,
                                'slidesToScroll' => 1,
                                'autoplay' => true,
                                'speed' => 1000,
                                'autoplaySpeed' => 3000,
                                'prevArrow' => false,
                                'nextArrow' => false,
                                'responsive' => [
                                    [
                                        'breakpoint' => 992,
                                        'settings' => [
                                            'slidesToShow' => 3
                                        ]
                                    ],
                                    [
                                        'breakpoint' => 768,
                                        'settings' => [
                                            'slidesToShow' => 2
                                        ]
                                    ],
                                    [
                                        'breakpoint' => 640,
                                        'settings' => [
                                            'slidesToShow' => 1
                                        ]
                                    ]
                                ]
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
            <!-- //similar -->
        <?php } ?>
    </div>
</div>
<!-- //product detail -->