<?php

use yii\helpers\Html;
use evgeniyrru\yii2slick\Slick;

/* @var $this yii\web\View */
/* @var $slider \backend\models\sliders\Sliders */
/* @var $about \backend\models\parts\HtmlParts */
/* @var $service \backend\models\parts\HtmlParts */
/* @var $recommend \backend\models\post\PostCategories */
/* @var $categories array */
/* @var $metaKeywords string */
/* @var $metaDescription string */
/* @var $footerText string */
/* @var $title string */
/* @var $logo string */

$this->title = $title;

$this->params['meta_type'] = 'page';
$this->params['meta_url'] = Yii::$app->request->hostInfo;
$this->params['meta_image'] = $logo;
if ($metaKeywords)
    $this->params['meta_keywords'] = $metaKeywords;
if ($metaDescription)
    $this->params['meta_description'] = $metaDescription;
if ($footerText)
    $this->params['footer_text'] = $footerText;

$this->params['background_line'] = true;
?>

<?php if ($slider != null && !empty($slider->sliderActiveItems)) { ?>
    <!-- banner -->
    <section class="banner">
        <?php
        $sliderItems = [];

        foreach ($slider->sliderActiveItems as $item) {
            array_push($sliderItems,
                (empty($item->link) || ($item->link == '#')) ?
                    Html::img($item->image, ['alt' => 'slider']) :
                    Html::a(Html::img($item->image, ['alt' => 'slider']), [$item->link]));
        }

        echo Slick::widget([
            'itemContainer' => 'div',
            'containerOptions' => ['class' => 'home-slider'],
            'items' => $sliderItems,
            'itemOptions' => ['class' => 'slide'],
            'clientOptions' => [
                'autoplay' => true,
                'speed' => 1000,
                'autoplaySpeed' => 3000,
                'prevArrow' => false,
                'nextArrow' => false,
            ],
        ]); ?>
    </section>
    <!-- //banner -->
<?php } ?>

<!-- about -->
<?php if ($about) echo $about->body; ?>
<!-- //about -->

<!-- service -->
<?php if ($service) echo $service->body; ?>
<!-- //service -->

<!-- recommend -->
<section class="recommend">
    <?php if ($recommend) { ?>
        <div class="top">
            <div class="container">
                <h2 class="title"><?= $recommend->name ?></h2>
                <div class="row" align="center">
                    <?php if ($recommend->homePagePosts) {
                        /** @var $post \backend\models\post\Posts */
                        foreach ($recommend->homePagePosts as $post) { ?>
                            <div class="col-sm-4 item">
                                <?= Html::img($post->image, ['alt' => 'post']) ?>
                                <div>
                                    <p><?= $post->title ?></p>
                                    <?= Html::a(Yii::t('frontend', 'Подробнее') . ' <i class="fa fa-angle-right"></i>',
                                        ['post/view', 'slug' => $post->slug]) ?>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    <?php }

    if ($categories) {
        /** @var $category \backend\models\product\Categories */
        foreach ($categories as $category) {
            if ($category->getCarouselItems()) { ?>
                <div class="carousel">
                    <div class="container">
                        <h3 class="title"><span><?= $category->name ?></span></h3>
                        <div class="body">
                            <?= Slick::widget([
                                'itemContainer' => 'div',
                                'items' => $category->getCarouselItems(),
                                'itemOptions' => ['class' => 'item'],
                                'clientOptions' => [
                                    'slidesToShow' => 4,
                                    'slidesToScroll' => 1,
                                    'autoplay' => true,
                                    'speed' => 1000,
                                    'autoplaySpeed' => 3000,
                                    'prevArrow' => '<i class="fa fa-chevron-left"></i>',
                                    'nextArrow' => '<i class="fa fa-chevron-right"></i>',
                                    'responsive' => [
                                        [
                                            'breakpoint' => 992,
                                            'settings' => [
                                                'slidesToShow' => 3
                                            ]
                                        ],
                                        [
                                            'breakpoint' => 768,
                                            'settings' => [
                                                'slidesToShow' => 2
                                            ]
                                        ],
                                        [
                                            'breakpoint' => 640,
                                            'settings' => [
                                                'slidesToShow' => 1
                                            ]
                                        ]
                                    ]
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            <?php }
        }
    } ?>

</section>
<!-- //recommend -->
