jQuery(document).ready(function () {

    /* Categories Menu Toggle */
    $('.categories-menu').find('button').click(function () {
        $(this).toggleClass('active');
        $($(this).data('toggle')).slideToggle('fast');
        $('#main_menu').removeClass('in');
    });

    $('.main-menu').find('button').click(function () {
        $('#categories_menu').hide();
    });

    $(document).on("click", function (event) {
        var trigger = $(".categories-menu");
        if (trigger !== event.target && !trigger.has(event.target).length) {
            trigger.find('button').removeClass('active');
            $('#categories_menu').slideUp("fast");
        }
    });

    // Add To Cart
    var overlay = jQuery('.overlay');

    function addToCart() {
        jQuery('.add-to-cart').click(function () {
            overlay.show();
            jQuery.ajax({
                type: "POST",
                url: '/customer/add-to-cart',
                data: {product: parseInt(jQuery(this).data('product'))},
                success: function (response) {
                    if (response === true) {
                        jQuery.pjax.reload({
                            container: "#header_basket"
                        }).done(function () {
                            overlay.hide();
                        });
                    }
                }
            });
        });
    }

    addToCart();

    jQuery(document).on('pjax:send', '#category_products', function () {
        overlay.show();
    });

    jQuery(document).on('pjax:complete', '#category_products', function () {
        overlay.hide();
        addToCart();
    });

});
