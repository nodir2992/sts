/**
 * Created by Nodir on 06.02.2018.
 */
(function ($) {
    $(document).ready(function () {

        $('.xzoom-img, .xzoom-gallery').xzoom({
            tint: '#d24242',
            defaultScale: -1,
            Xoffset: 10
        });

        //Integration with hammer.js
        var isTouchSupported = 'ontouchstart' in window;
        if (isTouchSupported) {
            //If touch device
            $('.xzoom-img').each(function () {
                var xzoom = $(this).data('xzoom');
                $(this).hammer().on("tap", function (event) {
                    event.pageX = event.gesture.center.pageX;
                    event.pageY = event.gesture.center.pageY;

                    xzoom.eventmove = function (element) {
                        element.hammer().on('drag', function (event) {
                            event.pageX = event.gesture.center.pageX;
                            event.pageY = event.gesture.center.pageY;
                            xzoom.movezoom(event);
                            event.gesture.preventDefault();
                        });
                    }

                    var counter = 0;
                    xzoom.eventclick = function (element) {
                        element.hammer().on('tap', function () {
                            counter++;
                            if (counter == 1) setTimeout(openmagnific, 300);
                            event.gesture.preventDefault();
                        });
                    }

                    function openmagnific() {
                        if (counter == 2) {
                            xzoom.closezoom();
                            var gallery = xzoom.gallery().cgallery;
                            var i, images = new Array();
                            for (i in gallery) {
                                images[i] = {src: gallery[i]};
                            }
                            $.magnificPopup.open({items: images, type: 'image', gallery: {enabled: true}});
                        } else {
                            xzoom.closezoom();
                        }
                        counter = 0;
                    }

                    xzoom.openzoom(event);
                });
            });

        } else {
            //If not touch device
            //Integration with magnific popup plugin
            $('#xzoom-magnific').bind('click', function (event) {
                var xzoom = $(this).data('xzoom');
                xzoom.closezoom();
                var gallery = xzoom.gallery().cgallery;
                var i, images = new Array();
                for (i in gallery) {
                    images[i] = {src: gallery[i]};
                }
                $.magnificPopup.open({
                    items: images,
                    type: 'image',
                    tClose: 'Закрыть (Esc)',
                    tLoading: 'Загружается...',
                    gallery: {
                        enabled: true,
                        tPrev: 'Предыдущий',
                        tNext: 'Следующий',
                        tCounter: '%curr% из %total%'
                    }
                });
                event.preventDefault();
            });
        }

    });
})(jQuery);
